<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $user = User::create(
            [
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'user_name' => 'Admin Admin',
                'email' => 'admin.citrusbug@gmail.com',
                'mobile' => '1234567890',
                'password' => bcrypt(123456),
                'status' => 1,
                // '_website_id' => 1,
                // 'api_token' => str_random(60)
            ]
        );

        if (!$user->hasRole('SU')) {
            $user->assignRole('SU');
        }
    }
}
