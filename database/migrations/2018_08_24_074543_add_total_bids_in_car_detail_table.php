<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalBidsInCarDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('TRUNCATE car_bids');
        Schema::table('car_detail', function (Blueprint $table) {
            $table->integer('total_bids')->default('0')->after('car_feature');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_detail', function (Blueprint $table) {
            $table->dropColumn('total_bids');
        });
    }
}
