<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id');
            $table->integer('model_id');
            $table->integer('fuel_type_id');
            $table->integer('abgasnorm_id');
            $table->integer('transmition_id');
            $table->integer('state_coach_id');
            $table->integer('country_id');
            $table->integer('state_id');
            $table->integer('city_id');
            $table->integer('created_by');
            $table->binary('first_registration');
            $table->string('type');
            $table->string('mfg_year');
            $table->string('engine_size');
            $table->string('engine_power');
            $table->string('co2_emmision');
            $table->string('mileage');
            $table->binary('all_wheel_drive');
            $table->integer('number_of_gears');
            $table->string('condition');
            $table->string('condition_pdf');
            $table->string('damage');
            $table->string('price');
            $table->text('description');
            $table->string('car_feature');
            $table->binary('status');
            $table->binary('bid_status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_detail');
    }
}
