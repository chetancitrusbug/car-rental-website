<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBidApproveUserIdInCarDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_detail', function (Blueprint $table) {
            $table->integer('approve_bid_user_id')->default('0')->after('total_bids');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('car_detail', function (Blueprint $table) {
            $table->dropColumn('approve_bid_user_id');
        });
    }
}
