<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('company_name')->after('password')->nullable();
            $table->string('legal_form')->after('company_name')->nullable();
            $table->string('tax_number')->after('legal_form')->nullable();
            $table->string('street')->after('tax_number')->nullable();
            $table->string('number')->after('street')->nullable();
            $table->string('post_code')->after('number')->nullable();
            $table->integer('country_id')->after('post_code')->nullable();
            $table->string('city')->after('country_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company_name');
            $table->dropColumn('legal_form');
            $table->dropColumn('tax_number');
            $table->dropColumn('street');
            $table->dropColumn('number');
            $table->dropColumn('post_code');
            $table->dropColumn('country_id');
            $table->dropColumn('city');
        });
    }
}
