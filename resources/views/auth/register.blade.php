@extends('layouts.frontend')
@section('page-title','REGISTER')

@section('content')
    <div class="choose-div-box m0 clearfix">
        <h1>Register</h1>
        <p class="p-heading">Registering is quick and allows you to begin submitting data. Car Rental will not provide your email address to any other party and will only notify you of account related activity.</p>
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="form-div clearfix">
            <div class="container">
                <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                    @csrf
                    <div class="form-register">
                        <div class="form-box clearfix">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group clearfix">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-5"><label for="username">First Name: </label></div>
                                            <div class="col-md-8 col-sm-7">
                                                <input id="first_name" type="text" class="form-control input-95 mb15 {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name" />
                                                <span class="req">*</span>

                                                @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- end of col -->
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-5"><label for="username">User Name: </label></div>
                                            <div class="col-md-8 col-sm-7">
                                                <input id="user_name" type="text" class="form-control input-95 mb15 {{ $errors->has('user_name') ? ' is-invalid' : '' }}" name="user_name" value="{{ old('user_name') }}" required autofocus placeholder="username" />
                                                <span class="req">*</span>

                                                @if ($errors->has('user_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('user_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- end of col -->
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-5"><label for="password">Password:</label></div>
                                            <div class="col-md-8 col-sm-7">
                                                <input id="password" type="password" class="form-control input-95  mb15 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="password" />
                                                <span class="req">*</span>

                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- end of col -->
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-4 col-sm-5"><label for="mobile">Mobile No: </label></div>
                                            <div class="col-md-8 col-sm-7">
                                                <input id="mobile" type="text" class="form-control input-95 mb15 {{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile') }}" required autofocus placeholder="Mobile" />
                                                <span class="req">*</span>

                                                @if ($errors->has('mobile'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('mobile') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div><!-- end of col -->
                            </div><!-- end of form-group -->
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group clearfix">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="last_name">Last Name:</label></div>
                                        <div class="col-md-8 col-sm-7">
                                            <input id="last_name" type="last_name" class="form-control input-95 mb15 {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required placeholder="Last Name" />

                                            <span class="req">*</span>
                                            @if ($errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('last_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="email">Email address:</label></div>
                                        <div class="col-md-8 col-sm-7">
                                            <input id="email" type="email" class="form-control input-95 mb15 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email address" />

                                            <span class="req">*</span>
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-5"><label for="password">Confirm password:</label></div>
                                        <div class="col-md-8 col-sm-7">
                                            <input id="password-confirm" type="password" class="form-control input-95  mb15" name="password_confirmation" required placeholder="Confirm Password" />
                                            <span class="req">*</span>
                                        </div>
                                    </div>
                                </div><!-- end of col -->
                            </div>
                        </div>
                    </div>
                </div><!-- End of form-register -->
            </div>

            <div class="form-group text-center mt30 clearfix">
                <div class="col-md-12 col-sm-12">
                     <input name="" type="submit" class="btn btn-primary text-center" value="Register">
                </div><!-- end of col -->
            </div><!-- end of form-group -->
        </form>

        <div class="clearfix text-center">
            <a href="{{ url('/') }}" class=""><i class="fa fa-angle-left "></i> Back to Home page</a></div>
        </div>
    </section><!-- end of content section -->
@endsection

