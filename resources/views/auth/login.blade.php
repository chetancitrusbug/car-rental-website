@extends('layouts.app')

@section('page-title','LOGIN')

@section('content')
    <form class="login100-form validate-form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf
        <span class="login100-form-title p-b-26">
            Welcome
        </span>
        @php
            $title = explode(' ',($setting['site name'])?$setting['site name']:'AUTO SQUARE');
            $first = $title[0];
            $second = implode(' ',array_except($title,['0']));
        @endphp
        <span class="login100-form-title p-b-48">
            <strong class="text-primary">{{$first}}</strong><span class="text-secondary">{{$second}}</span>
        </span>

        <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
            <input class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" name="email" value="{{ old('email') }}" placeholder="Email">
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong class="error">{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <span class="btn-show-pass">
                <i class="zmdi zmdi-eye"></i>
            </span>
            <input class="input100" type="password" name="password" placeholder="Password">
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong class="error">{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
        <div class="g-recaptcha" data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}"></div>

        <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button class="login100-form-btn">
                    Sign In
                </button>
            </div>
        </div>

        <div class="text-center p-t-115">
            <a class="txt2" href="{{ route('password.request') }}">
                Forgot Password
            </a>
        </div>
    </form>
@endsection


