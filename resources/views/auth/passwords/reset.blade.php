@extends('layouts.app')

@section('content')
    <form class="login100-form validate-form" method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <span class="login100-form-title p-b-26">
            Reset Password
        </span>

        <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
            <input class="input100 {{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" name="email" value="{{ old('email') }}" required placeholder="Email">
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong class="error">{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <span class="btn-show-pass">
                <i class="zmdi zmdi-eye"></i>
            </span>
            <input class="input100" type="password" name="password" required placeholder="Password">
            @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong class="error">{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="wrap-input100 validate-input" data-validate="Enter password">
            <span class="btn-show-pass">
                <i class="zmdi zmdi-eye"></i>
            </span>
            <input class="input100" type="password" name="password_confirmation" required placeholder="Confirm Password">
            <span class="focus-input100" data-placeholder="Confirm Password"></span>
        </div>

        <div class="container-login100-form-btn">
            <div class="wrap-login100-form-btn">
                <div class="login100-form-bgbtn"></div>
                <button class="login100-form-btn">
                    {{ __('Reset Password') }}
                </button>
            </div>
        </div>
    </form>
@endsection
