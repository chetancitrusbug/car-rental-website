@extends('layouts.frontend')
@push('css')
    <style>
        .container-outer{ min-height: 800px; }
    </style>
@endpush
@section('content')
<div class="container-outer pb-20">
    <div class="container">
        <div class="contact-wrapper GridLex-gap-30">
            <div class="GridLex-grid-noGutter-equalHeight">
                <div class="GridLex-col-6_sm-4_xs-12" style="margin:0 auto;">
                    <div class="contact-form-wrapper-boxed">
                        <div class="section-title text-left mb-20">
                            <h4>Edit Profile</h4>
                            <p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> These fields are required.</p>
                        </div>
                        <form id="contact-form" method="post" action="{{route('user-profile-update',$user->id)}}">
                            @csrf
                            <div class="messages"></div>

                            <div class="row gap-20">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <p>{{$user->email}}</p>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>* First Name</label>
                                        <input class="form-control" placeholder="Enter First Name" type="text" name="first_name" value="{{old('first_name',$user->first_name)}}">
                                        {!! $errors->first('first_name', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>* Last Name</label>
                                        <input class="form-control" placeholder="Enter Last Name" type="text" name="last_name" value="{{old('last_name',$user->last_name)}}">
                                        {!! $errors->first('last_name', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>* User Name</label>
                                        <input class="form-control" placeholder="Enter User Name" type="text" name="user_name" value="{{old('user_name',$user->user_name)}}">
                                        {!! $errors->first('user_name', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>* Mobile</label>
                                        <input class="form-control" placeholder="Enter Mobile Number" type="text" name="mobile" value="{{old('mobile',$user->mobile)}}">
                                        {!! $errors->first('mobile', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12">
                                    <input type="hidden" name="is_register" value="1">
                                    <input type="submit" class="btn btn-primary btn-send mt-10" value="Update">
                                    <a type="button" class="btn btn-warning btn-send mt-10" href="{{route('my-account')}}">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


