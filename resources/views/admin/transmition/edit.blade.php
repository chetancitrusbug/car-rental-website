@extends('layouts.admin') 
@section('title',"Edit Transmition") 
@push('css')
    <style type="text/css">
        .logo{
            position: relative;
        }.logo-image{
            position: absolute;
            left: 490px;
            top: 250px;
        }img{
            height: 50px;
            width: 50px;
        }
    </style>
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Edit Transmition  # {{$transmition->title}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/admin/transmition') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::model($transmition, ['method' => 'PATCH','url' => ['/admin/transmition', $transmition->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                    @include ('admin.transmition.form')

                    <div class="form-group">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection