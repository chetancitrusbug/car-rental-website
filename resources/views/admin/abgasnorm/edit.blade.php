@extends('layouts.admin') 
@section('title',"Edit Abgasnorm") 
@push('css')
    <style type="text/css">
        .logo{
            position: relative;
        }.logo-image{
            position: absolute;
            left: 490px;
            top: 250px;
        }img{
            height: 50px;
            width: 50px;
        }
    </style>
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Edit Abgasnorm  # {{$abgasnorm->title}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/admin/abgasnorm') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::model($abgasnorm, ['method' => 'PATCH','url' => ['/admin/abgasnorm', $abgasnorm->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                    @include ('admin.abgasnorm.form')

                    <div class="form-group">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection