<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', '* Select Brand: ', ['class' => 'control-label']) !!} 
    {!! Form::select('brand_id', [''=>'-- Select Brand --']+$brandList, null ,['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Model Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('name', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!} 
    {!! Form::checkbox('status',1, isset($carModel->status)?$carModel->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
