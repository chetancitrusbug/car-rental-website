@extends('layouts.admin') 
@section('title',"Create Car model") 
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Create New Car Model</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                    <a href="{{ url('/admin/car-model') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::open(['url' => '/admin/car-model', 'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                    @include ('admin.car_model.form')
                    
                    <div class="form-group">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection