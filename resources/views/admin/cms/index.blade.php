@extends('layouts.admin')
@section('title',"CMS | ")
@section('content')

<div class="card">
    @php
        $status = config('constants.cms.status');
    @endphp
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">CMS</h4>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/cms/create') }}" class="btn btn-success btn-sm" title="Add New CMS">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div> --}}
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="cms-table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        var url ="{{ url('/admin/cms-data') }}";
        var edit_url = "{{ url('/admin/cms') }}";

        datatable = $('#cms-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'title',name : 'title',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": true,
                    "orderable": false,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' checked data-table='cms' data-status="+o.status+" onchange=statusChange() value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-table='cms' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e = v = d = m = "";
                        e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";
                        /* d = "<a href='{{url('admin/cms')}}"+"/"+o.id+"' class='del-item btn btn-danger btn-sm' data-method='DELETE' data-modal-text='delete cms' data-id="+o.id+" ><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;"; */
                        return m+v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/cms')}}/" + id;
            var r = confirm("Are you sure you want to Delete CMS?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $(document).on('change', '.status-change', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var table = $(this).data('table');

            url = "{{url('admin/change-status')}}/" + table + '/' + status + '/' + id;

            $.ajax({
                url: url ,
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });

    </Script>
@endpush