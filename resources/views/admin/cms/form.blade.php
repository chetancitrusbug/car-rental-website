@push('css')
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.css" rel="stylesheet">
    <style>
        /* .popover { top: auto; left: auto } */
    </style>
@endpush
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'control-label']) !!}
    {!! Form::text('title', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
    {!! $errors->first('title', '<p class="help-block with-errors">:message</p>') !!}
    {!! $errors->first('slug', '<p class="help-block with-errors">:message</p>') !!}
</div>
@if(in_array($cm->slug,['why-autosquare','recently-added-cars','about-us-more-than-car-dealer-websites','browser-by-brands','about-us']))
    <div class="form-group {{ $errors->has('sub_title') ? 'has-error' : ''}}">
        {!! Form::label('sub_title', 'Sub Title', ['class' => 'control-label']) !!}
        {!! Form::textarea('sub_title', null, ['class' => 'form-control col-md-4']) !!}
        {!! $errors->first('sub_title', '<p class="help-block with-errors">:message</p>') !!}
    </div>
@endif

@if(in_array($cm->slug,['why-autosquare','who-are-we','what-do-we-do','welcome-to-autosquare','terms-and-conditions','privacy-policy','about-us-our-mission','home-page-header']))
    <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
        {!! Form::label('description', 'Description', ['class' => 'control-label']) !!}
        {!! Form::textarea('description', null, ['class' => 'form-control col-md-4 summernote']) !!}
        {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
@endif

@if(in_array($cm->slug,['who-are-we','welcome-to-autosquare','about-us-our-mission','about-us-more-than-car-dealer-websites','home-page-header']))
    <div class="form-group logo {{ $errors->has('image') ? 'has-error' : ''}}">
        {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
        {!! Form::file('image', null, ['class' => 'form-control col-md-4','onchange'=>'changeImage(1)']) !!}
        @if(isset($cm) && !empty($cm->image))
            <div class="logo-image"><img src="{!! asset('/uploads/cms/'.$cm->image) !!}" class="changeImage1"></div>
        @endif
        {!! $errors->first('image', '<p class="help-block with-errors">:message</p>') !!}
    </div>
@endif

@if(in_array($cm->slug,['recently-added-cars','home-page-header','browser-by-brands']))
    <div class="form-group {{ $errors->has('button_title') ? 'has-error' : ''}}">
        {!! Form::label('button_title', 'Button Title', ['class' => 'control-label']) !!}
        {!! Form::text('button_title', null, ['class' => 'form-control col-md-4']) !!}
        {!! $errors->first('button_title', '<p class="help-block with-errors">:message</p>') !!}
    </div>
    <div class="form-group {{ $errors->has('button_link') ? 'has-error' : ''}}">
        {!! Form::label('button_link', 'Button Link', ['class' => 'control-label']) !!}
        {!! Form::text('button_link', null, ['class' => 'form-control col-md-4']) !!}
        {!! $errors->first('button_link', '<p class="help-block with-errors">:message</p>') !!}
    </div>
@endif

<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($cm->status)?$cm->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

@push('js')
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.7/summernote.js"></script>
    <script>
        $(document).ready(function(){
            $('#title').focus();
            $('.note-popover').css({'display': 'none'});

            $('#image').on('change',function(){
                $('.logo-image').hide();
            });


        });
            $('.summernote').summernote({
                height: 250,                 // set editor height
                width: 1000,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                dialogsInBody: true,
                dialogsFade: false,
                /* focus: true,                  // set focus to editable area after initializing summernote
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete: function($target, editor, $editable) {
                        deleteImage($($target[0]).attr("data-name"));
                        $target.remove();
                    }
                } */
                popover: {
                    image: [
                        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                        ['float', ['floatLeft', 'floatRight', 'floatNone']],
                        ['remove', ['removeMedia']]
                    ],
                    link: [
                        ['link', ['linkDialogShow', 'unlink']]
                    ],
                    air: [
                        ['color', ['color']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['para', ['ul', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']]
                    ]
                    }
            });

    </script>
@endpush
