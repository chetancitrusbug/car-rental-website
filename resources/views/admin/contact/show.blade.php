@extends('layouts.admin')
@section('title',"Roles")
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title"> Contact Us Detail # {{ $contact->name }} </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/admin/contact') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                    {!! Form::open(['method' => 'DELETE','url' => ['/admin/contact', $contact->id],'style' => 'display:inline']) !!}
                        <input type="hidden" name="view" value="1">
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array('type' => 'submit','class' => 'btn btn-danger btn-xs','title' => 'Delete Role','onclick'=>"return confirm('Cofirm Delete?')"))!!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <td>Name</td>
                            <td> {{ $contact->name }} </td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{{ $contact->email }}</td>
                        </tr>
                        <tr>
                            <td>Title</td>
                            <td> {{ $contact->title }} </td>
                        </tr>
                        <tr>
                            <td>Message</td>
                            <td> {{ $contact->message }} </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection