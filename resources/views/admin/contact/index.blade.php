@extends('layouts.admin')
@section('title',"Contact | ")
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Contact</h4>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="contact-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>email</th>
                        <th>Title</th>
                        <th>Create time</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/contact-data') }}";
        var contact_url = "{{ url('/admin/contact') }}";

        datatable = $('#contact-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            columns: [
                { data: 'name',name : 'name',"searchable": true, "orderable": false},
                { data: 'email',name : 'email',"searchable": true, "orderable": false},
                { data: 'title',name : 'title',"searchable": true, "orderable": false},
                { data: 'created_atdate',name : 'created_atdate',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e = v = d = m = "";
                        v = "<a href='"+contact_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+"><button class='btn btn-primary btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";
                        d = "<a href='{{url('admin/contact')}}"+"/"+o.id+"' class='del-item btn btn-danger btn-sm' data-method='DELETE' data-modal-text='delete contact' data-id="+o.id+" ><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;";
                        return m+v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/contact')}}/" + id;
            var r = confirm("Are you sure you want to Delete Contact?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });
    </Script>
@endpush