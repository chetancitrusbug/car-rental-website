<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Engine Name: ', ['class' => 'control-label']) !!} 
    {!! Form::text('name', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!} 
    {!! Form::checkbox('status',1, isset($carEngine->status)?$carEngine->status:1, ['class'=>['checkbox']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
