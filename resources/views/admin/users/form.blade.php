<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', '* First Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
    {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', '* Last Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
    {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('user_name') ? ' has-error' : ''}}">
    {!! Form::label('user_name', '* User Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('user_name', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!} {!! $errors->first('user_name', '
    <p class="help-block">:message</p>') !!}
</div>
@if(isset($user->email))
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!}
        {!! Form::text('email', null, ['class' => 'form-control col-md-4','disabled' => 'disabled']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
@else
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!}
        {!! Form::text('email', null, ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
        {!! Form::label('password', '* Password: ', ['class' => 'control-label']) !!}
        {!! Form::password('password',  ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ''}}">
        {!! Form::label('password_confirmation', '* Password Confirm: ', ['class' => 'control-label']) !!}
        {!! Form::password('password_confirmation',  ['class' => 'form-control col-md-4', 'required' => 'required']) !!}
        {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
    </div>
@endif
<div class="form-group{{ $errors->has('mobile') ? ' has-error' : ''}}">
    {!! Form::label('mobile', '* Mobile: ', ['class' => 'control-label']) !!}
    {!! Form::text('mobile', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('company_name') ? ' has-error' : ''}}">
    {!! Form::label('company_name', '* Name Of Company: ', ['class' => 'control-label']) !!}
    {!! Form::text('company_name', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('company_name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('legal_form') ? ' has-error' : ''}}">
    {!! Form::label('legal_form', '* Legal Form Of The Company: ', ['class' => 'control-label']) !!}
    {!! Form::file('legal_form', array('class' => 'form-control-file col-md-4 pdf','onchange'=>'changeImage(1)')) !!}
    @if(isset($user))
        <div class="pdf-file">{{ $user->legal_form }}</div>
    @endif
    {!! $errors->first('legal_form', '<p class="help-block">:message</p>') !!}
</div>
{!! Form::hidden('old_legal_form', isset($user->legal_form)?$user->legal_form:'') !!}
<div class="form-group{{ $errors->has('tax_number') ? ' has-error' : ''}}">
    {!! Form::label('tax_number', '* Tax Number: ', ['class' => 'control-label']) !!}
    {!! Form::text('tax_number', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('tax_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('street') ? ' has-error' : ''}}">
    {!! Form::label('street', '* Street: ', ['class' => 'control-label']) !!}
    {!! Form::text('street', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('street', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number') ? ' has-error' : ''}}">
    {!! Form::label('number', '* Number: ', ['class' => 'control-label']) !!}
    {!! Form::text('number', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('post_code') ? ' has-error' : ''}}">
    {!! Form::label('post_code', '* Postcode: ', ['class' => 'control-label']) !!}
    {!! Form::text('post_code', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('post_code', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country_id') ? ' has-error' : ''}}">
    {!! Form::label('country_id', '* Select Country: ', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', [''=>'-- Select Country --']+$countriesList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('city') ? ' has-error' : ''}}">
    {!! Form::label('city', '* Select City: ', ['class' => 'control-label']) !!}
    {!! Form::text('city', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($user->status)?$user->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
</div>