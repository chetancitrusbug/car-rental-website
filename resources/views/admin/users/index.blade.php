@extends('layouts.admin')
@section('title',"Users")
@section('content')

<div class="card">
    @php
        $status = config('constants.users.status');
    @endphp
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Users</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/users/create') }}" class="btn btn-success btn-sm" title="Add New User">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="users-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/users-data') }}";
        var edit_url = "{{ url('/admin/users') }}";
        var auth_check = "{{ Auth::check() }}";

        datatable = $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: 'id',name : 'id',"searchable": false, "orderable": false},
                { data: 'user_name',name : 'user_name',"searchable": true, "orderable": false},
                { data: 'email',name : 'email',"searchable": true, "orderable": false},
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' checked data-table='users' data-status="+o.status+" onchange=statusChange() value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-table='users' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+edit_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-primary btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";
                        if(o.id != 1){
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";
                            d = "<a href='javascript:void(0);' class='del-item' data-id="+o.id+" ><button class='btn btn-danger btn-sm' title='Delete' ><i class='fa fa-trash-o' aria-hidden='true'></i></button></a>&nbsp;";
                        }
                        return v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/users')}}/" + id;
            var r = confirm("Are you sure you want to Delete User?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    data: {from_index: true},
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $('#areas').change(function() {
            datatable.draw();
        });

        $(document).on('change', '.status-change', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var table = $(this).data('table');

            url = "{{url('admin/change-status')}}/" + table + '/' + status + '/' + id;

            $.ajax({
                url: url ,
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });
    </Script>
@endpush
