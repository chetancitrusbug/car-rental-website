@extends('layouts.admin')
@section('title',"View User")
@section('content')

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title"> User # {{ $user->name }} </h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                @if($user->id != 0)
                    <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit user">
                        <button class="btn btn-primary btn-xs">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                        </button>
                    </a>

                    {!! Form::open(['method' => 'DELETE','url' => ['/admin/users', $user->id],'style' => 'display:inline']) !!}
                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array('type' => 'submit','class' => 'btn btn-danger btn-xs','title' => 'Delete user','onclick'=>"return confirm('Cofirm Delete?')"))!!}
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td>Id</td>
                        <td>{{ $user->id }}</td>
                    </tr>
                    <tr>
                        <td>User Name</td>
                        <td> {{ $user->user_name }} </td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td> {{ $user->email }} </td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td> {{ $user->mobile }} </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    @if(count($user->bids))
        <div class="card-body">
            <div class="card-block">
                <table class="table table-borderless">
                    <tbody>
                        <thead>
                            <tr>
                                <th></th>
                                <th>Brand</th>
                                <th>Model</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($user->bids as $key => $value)
                                <tr>
                                    <td> {{ $key+1 }} </td>
                                    <td> {{ $value->brand_name }} </td>
                                    <td> {{ $value->model_name }} </td>
                                    <td> €{{ $value->price }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </tbody>
                </table>
            @endif
        </div>
    </div>
</div>
@endsection