@extends('layouts.admin') 
@section('title',"Edit Users") 
@push('css')
    <style type="text/css">
        .pdf{
            position: relative;
        }.pdf-file{
            position: absolute;
            left: 490px;
            top: 738px;
        }img{
            height: 50px;
            width: 50px;
        }
    </style>
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Edit User  # {{$user->user_name}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::model($user, ['method' => 'PATCH','url' => ['/admin/users', $user->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off','files'=>true]) !!}

                    @include ('admin.users.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection