<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', '* Title: ', ['class' => 'control-label']) !!} 
    {!! Form::text('title', null, ['class' => 'form-control col-md-4']) !!} 
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!} 
    {!! Form::checkbox('status',1, isset($fuelType->status)?$fuelType->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
