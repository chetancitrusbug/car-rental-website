@extends('layouts.admin')
@section('title',"Car Bids | ")

@push('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')

<div class="card">
    @php
        $status = config('constants.car_bids.status');
    @endphp
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Bid List</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/car-detail') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless">
                <tbody>

                    <tr>
                        <td>Brand</td>
                        <td> {{ $carDetail->brand_name }} </td>
                    </tr>
                    <tr>
                        <td>Model</td>
                        <td> {{ $carDetail->model_name }} </td>
                    </tr>
                    <tr>
                        <td>Type</td>
                        <td> {{ $carDetail->type }} </td>
                    </tr>
                    <tr>
                        <td>Price</td>
                        <td> €{{ $carDetail->price }} </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="car-bids-table">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Mobile No</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/car-bid-data/'.$id) }}";

        datatable = $('#car-bids-table').DataTable({
            processing: true,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
            },
                columns: [

                    { data: 'user_name',name : 'user_name',"searchable": true, "orderable": true},
                    { data: 'mobile',name : 'mobile',"searchable": false, "orderable": true},
                    { data: 'price',name : 'price',"searchable": true, "orderable": true,"render": function (o) {
                            return '€'+o;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "width":150,
                        "render": function (o) {
                            if(o.status == ""){
                                return "<a href='#' value="+o.id+" data-id="+o.id+" data-car-detail-id="+o.car_detail_id+" data-brand-id="+o.brand_id+" data-model-id="+o.model_id+" class='btn btn-primary approve-bid btn-sm'>Approve</a>&nbsp;";
                            }else{
                                if(o.status == '1'){
                                    return '<span style="color:green;">Approved</span>';
                                }
                                if(o.status == '0'){
                                    return '<span style="color:red;">Reject</span>';
                                }
                            }
                        }

                    }
                ]
        });

        $(document).on('click', '.approve-bid', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var car_detail_id = $(this).data('car-detail-id');
            var brand_id = $(this).data('brand-id');
            var model_id = $(this).data('model-id');

            url = "{{url('admin/car-bid-approve')}}";

            $.ajax({
                url: url ,
                type: 'POST',
                data : {bid_id: id, car_detail_id: car_detail_id, brand_id:brand_id, model_id:model_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();

                    location.reload();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });
    </Script>
@endpush