@push('css')
    <link rel="stylesheet" href="{{asset('date/bootstrap-datepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('date/bootstrap-datepicker.standalone.min.css')}}">

    <style type="text/css">
        .imageThumb {
            max-height: 75px;
            border: 2px solid grey;
            margin: 10px 10px 0 0;
            padding: 1px;
         }
    </style>
@endpush
{{ Form::hidden('created_by', \Auth::getUser()->id) }}
<div class="form-group{{ $errors->has('brand_id') ? ' has-error' : ''}}">
    {!! Form::label('brand_id', '* Select Brand: ', ['class' => 'control-label']) !!}
    {!! Form::select('brand_id', [''=>'-- Select Brand --']+$brandList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('brand_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('model_id') ? ' has-error' : ''}}">
    {!! Form::label('model_id', '* Select Model: ', ['class' => 'control-label']) !!}
    {!! Form::select('model_id', [''=>'-- Select Model --']+$modelList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('model_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('fuel_type_id') ? ' has-error' : ''}}">
    {!! Form::label('fuel_type_id', '* Select Fuel Type: ', ['class' => 'control-label']) !!}
    {!! Form::select('fuel_type_id', [''=>'-- Select Fuel Type --']+$fuelTypeList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('fuel_type_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('abgasnorm_id') ? ' has-error' : ''}}">
    {!! Form::label('abgasnorm_id', '* Select Abgasnorm: ', ['class' => 'control-label']) !!}
    {!! Form::select('abgasnorm_id', [''=>'-- Select Abgasnorm --']+$abgasnormList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('abgasnorm_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('transmition_id') ? ' has-error' : ''}}">
    {!! Form::label('transmition_id', '* Select Transmition: ', ['class' => 'control-label']) !!}
    {!! Form::select('transmition_id', [''=>'-- Select Transmition --']+$transmitionList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('transmition_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('state_coach_id') ? ' has-error' : ''}}">
    {!! Form::label('state_coach_id', '* Select State Coach: ', ['class' => 'control-label']) !!}
    {!! Form::select('state_coach_id', [''=>'-- Select State Coach --']+$stateCoachList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('state_coach_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('country_id') ? ' has-error' : ''}}">
    {!! Form::label('country_id', '* Select Country: ', ['class' => 'control-label']) !!}
    {!! Form::select('country_id', [''=>'-- Select Country --']+$countriesList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('country_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('state_id') ? ' has-error' : ''}}">
    {!! Form::label('state_id', '* Select State: ', ['class' => 'control-label']) !!}
    {!! Form::select('state_id', [''=>'-- Select State --']+$statesList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('state_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('city_id') ? ' has-error' : ''}}">
    {!! Form::label('city_id', '* Select City: ', ['class' => 'control-label']) !!}
    {!! Form::select('city_id', [''=>'-- Select City --']+$citiesList, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('registration_date') ? ' has-error' : ''}}">
    {!! Form::label('registration_date', '* Registration Date: ', ['class' => 'control-label']) !!}
    {!! Form::text('registration_date', null, ['class' => 'form-control col-md-4 datetimepicker']) !!}
    {!! $errors->first('registration_date', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
    {!! Form::label('type', '* Type: ', ['class' => 'control-label']) !!}
    {!! Form::text('type', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('vin_number') ? ' has-error' : ''}}">
    {!! Form::label('vin_number', '* Vin Number: ', ['class' => 'control-label']) !!}
    {!! Form::text('vin_number', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('vin_number', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mfg_year') ? ' has-error' : ''}}">
    {!! Form::label('mfg_year', '* Select Manufacturing Year: ', ['class' => 'control-label']) !!}
    {!! Form::select('mfg_year', [''=>'-- Select Manufacturing Year --']+$mfgYear, null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('mfg_year', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('engine_size') ? ' has-error' : ''}}">
    {!! Form::label('engine_size', '* Engine Size: ', ['class' => 'control-label']) !!}
    {!! Form::text('engine_size', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('engine_size', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('engine_power') ? ' has-error' : ''}}">
    {!! Form::label('engine_power', '* Engine Power: ', ['class' => 'control-label']) !!}
    {!! Form::text('engine_power', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('engine_power', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('co2_emmision') ? ' has-error' : ''}}">
    {!! Form::label('co2_emmision', '* Co2 Emmision: ', ['class' => 'control-label']) !!}
    {!! Form::text('co2_emmision', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('co2_emmision', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('mileage') ? ' has-error' : ''}}">
    {!! Form::label('mileage', '* Kilometer: ', ['class' => 'control-label']) !!}
    {!! Form::text('mileage', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('mileage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('number_of_gears') ? ' has-error' : ''}}">
    {!! Form::label('number_of_gears', '* Select Number Of Gears: ', ['class' => 'control-label']) !!}
    {!! Form::select('number_of_gears', [''=>'-- Select Number Of Gears --']+config('constants.car_detail.gears'), null ,['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('number_of_gears', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('condition') ? ' has-error' : ''}}">
    {!! Form::label('condition', '* Condition: ', ['class' => 'control-label']) !!}
    {!! Form::text('condition', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('condition', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('condition_pdf') ? ' has-error' : ''}}">
    {!! Form::label('condition_pdf', '* Upload Condition PDF: ', ['class' => 'control-label']) !!}
    {!! Form::file('condition_pdf', array('class' => 'form-control-file col-md-4 pdf','onchange'=>'changeImage(1)')) !!}
    @if(isset($carDetail))
        <div class="pdf-file">{{ $carDetail->condition_pdf }}</div>
    @endif
    {!! $errors->first('condition_pdf', '<p class="help-block">:message</p>') !!}
</div>
{!! Form::hidden('old_condition_pdf', isset($carDetail->condition_pdf)?$carDetail->condition_pdf:'') !!}
<div class="form-group{{ $errors->has('damage') ? ' has-error' : ''}}">
    {!! Form::label('damage', '* Damage: ', ['class' => 'control-label']) !!}
    {!! Form::text('damage', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('damage', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('price') ? ' has-error' : ''}}">
    {!! Form::label('price', '* Price: ', ['class' => 'control-label']) !!}
    {!! Form::text('price', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
    {!! Form::label('description', '* Description: ', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('car_feature') ? ' has-error' : ''}}">
    {!! Form::label('car_feature', '* Select Car Feature: ', ['class' => 'control-label']) !!}
    @php
        $feature = (isset($carDetail))?explode(',', $carDetail->car_feature):[];
    @endphp
    <div class="col-md-12">
        @foreach($carFeatureList as $key => $value)
            <div class="checkbox">
                <label><input type="checkbox" name="car_feature[]" value="{{ $key }}" {{ (in_array($key, $feature))?'checked':'' }}>{{ $value }}</label>
            </div>
        @endforeach
    </div>
    {!! $errors->first('car_feature', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('first_registration') ? ' has-error' : ''}}">
    {!! Form::label('first_registration', 'First Registration: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('first_registration',1, isset($carDetail->first_registration)?$carDetail->first_registration:1, ['class'=>['checkbox','status1']]) !!}
    {!! $errors->first('first_registration', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('all_wheel_drive') ? ' has-error' : ''}}">
    {!! Form::label('all_wheel_drive', 'All Wheel Drive: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('all_wheel_drive',1, isset($carDetail->all_wheel_drive)?$carDetail->all_wheel_drive:1, ['class'=>['checkbox','status1']]) !!}
    {!! $errors->first('all_wheel_drive', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($carDetail->status)?$carDetail->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('bid_status') ? ' has-error' : ''}}">
    {!! Form::label('bid_status', 'Bid Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('bid_status',1, isset($carDetail->bid_status)?$carDetail->bid_status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('bid_status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
    {!! Form::label('image', '* Upload Car Images: ', ['class' => 'control-label']) !!}
    {!! Form::file('image[]', array('class' => 'form-control-file col-md-4 image1','onchange'=>'changeImage(2)','multiple')) !!}
    <div class="multipleImage">
        @if(isset($carDetail))
            @foreach($carDetail->images as $value)
                <img src="{{ asset('uploads/car_detail/image/thumbnail/'.$value['image']) }}" class="changeImage1 imageThumb">
            @endforeach
        @endif
    </div>
    @if($errors->has('image.*'))
        <p class="help-block">Only jpg,jpeg or png files allowed</p>
    @endif
</div>

@push('js')
    <script src="{{asset('date/bootstrap-datepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function($) {
            $('.datetimepicker').datepicker({
                format:'dd-mm-yyyy'
            });

            if(window.File && window.FileList && window.FileReader) {
                $("input[name='image[]']").on("change",function(e) {
                    var files = e.target.files ,
                    filesLength = files.length ;
                    $(".multipleImage").html('');
                    for (var i = 0; i < filesLength ; i++) {
                        var f = files[i]
                        var fileReader = new FileReader();
                        fileReader.onload = (function(e) {
                            var file = e.target;
                            $("<img></img>",{
                                class : "imageThumb",
                                src : e.target.result,
                                title : file.name
                            }).appendTo(".multipleImage");
                        });
                        fileReader.readAsDataURL(f);
                    }
                });
            }

            //model dependency on brand change
            $('select[name="brand_id"]').on('change', function(event) {
                $.ajax({
                    url: "{{ url('/admin/car-detail-model') }}",
                    data: {brand_id: $(this).val()},
                })
                .done(function(data) {
                    $('select[name="model_id"] option:not(:first)').remove();
                    if(data != ''){
                        $.each(data, function(index, val) {
                            $('select[name="model_id"]').append("<option value='"+index+"'>"+val+"</option>");
                        });
                    }
                });
            });

            //state dependency on country change
            $('select[name="country_id"]').on('change', function(event) {
                $.ajax({
                    url: "{{ url('/admin/car-detail-state') }}",
                    data: {country_id: $(this).val()},
                })
                .done(function(data) {
                    $('select[name="state_id"] option:not(:first)').remove();
                    if(data != ''){
                        $.each(data, function(index, val) {
                            $('select[name="state_id"]').append("<option value='"+index+"'>"+val+"</option>");
                        });
                    }
                });
            });

            //country dependency on state change
            $('select[name="state_id"]').on('change', function(event) {
                $.ajax({
                    url: "{{ url('/admin/car-detail-city') }}",
                    data: {state_id: $(this).val()},
                })
                .done(function(data) {
                    $('select[name="city_id"] option:not(:first)').remove();
                    if(data != ''){
                        $.each(data, function(index, val) {
                            $('select[name="city_id"]').append("<option value='"+index+"'>"+val+"</option>");
                        });
                    }
                });
            });

        });
    </script>
@endpush