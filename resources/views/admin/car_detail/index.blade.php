@extends('layouts.admin')
@section('title',"Car Detail | ")
@section('content')

<div class="card">
    @php
        $status = config('constants.car_detail.status');
    @endphp
    <div class="card-header">
        <div class="row">
            <div class="col-8">
                <h4 class="card-title">Car</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <a href="{{ url('/admin/car-detail/create') }}" class="btn btn-success btn-sm" title="Add New Car Detail">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-block">
            <table class="table table-borderless" style="width:100%;" id="car-detail-table">
                <thead>
                    <tr>
                        <th>Car Brand</th>
                        <th>Car Model</th>
                        <th>Type</th>
                        <th>Price</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
@endsection
@push('js')
    <script>
        var url ="{{ url('/admin/car-detail-data') }}";
        var edit_url = "{{ url('/admin/car-detail') }}";
        var bid_url = "{{ url('/admin/car-bid-list') }}";

        datatable = $('#car-detail-table').DataTable({
            processing: false,
            serverSide: true,
            "caseInsensitive": false,
            "order": [[0,"desc"]],
            ajax: {
                url:url,
                type:"get",
                data: {brand_id:{{ request()->get('brand_id',0) }}, model_id:{{ request()->get('model_id',0) }}}
            },
            "drawCallback": function( settings ) {
                statusChange();
            },
            columns: [
                { data: null,name : 'brand_name',"searchable": true, "orderable": false, "render": function(o){
                        return o.brand_name + ' - ' + o.bid_count + ' bids';
                    }
                },
                { data: 'model_name',name : 'model_name',"searchable": true, "orderable": false},
                { data: 'type',name : 'type',"searchable": true, "orderable": false},
                { data: 'price',name : 'price',"searchable": true, "orderable": false,"render": function (o) {
                        return '€'+o;
                    }
                },
                {
                    "data": null,
                    "name" : 'status',
                    "searchable": true,
                    "orderable": false,
                    "render": function (o) {
                        if(o.status == 1){
                            return "<input type='checkbox' class='status status-change' checked data-table='car_detail' data-status="+o.status+" onchange=statusChange() value="+o.id+" data-id="+o.id+">&nbsp;";
                        }
                        return "<input type='checkbox' class='status status-change' data-table='car_detail' data-status="+o.status+" value="+o.id+" data-id="+o.id+">&nbsp;";
                    }
                },
                {
                    "data": null,
                    "searchable": false,
                    "orderable": false,
                    "width":150,
                    "render": function (o) {
                        var e=""; var v=""; var d= "";
                        v = "<a href='"+bid_url+"/"+o.id+"' value="+o.id+" data-id="+o.id+" ><button class='btn btn-primary btn-sm' title='View' ><i class='fa fa-eye' ></i></button></a>&nbsp;";
                        if(o.brand_status == '1'
                            && o.model_status == '1'
                            /* && o.fuel_type_status == '1'
                            && o.abgasnorm_status == '1'
                            && o.state_coach_status == '1'
                            && o.transmition_status == '1' */
                        ){
                            e = "<a href='"+edit_url+"/"+o.id+"/edit' value="+o.id+" data-id="+o.id+" ><button class='btn btn-info btn-sm' title='Edit' ><i class='fa fa-edit' ></i></button></a>&nbsp;";
                        }
                        d = "<a href='{{url('admin/car-detail')}}"+"/"+o.id+"' class='del-item btn btn-danger btn-sm' data-method='DELETE' data-modal-text='delete car detail' data-id="+o.id+" ><i class='fa fa-trash-o' aria-hidden='true'></i></a>&nbsp;";
                        return v+e+d;
                    }

                }
            ]
        });

        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            url = "{{url('admin/car-detail')}}/" + id;
            var r = confirm("Are you sure you want to Delete Car detail?");

            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success(data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procced!',erro)
                    }
                });
            }
        });

        $(document).on('change', '.status-change', function (e) {
            $("#loading").show();
            var id = $(this).data('id');
            var status = $(this).data('status');
            var table = $(this).data('table');

            url = "{{url('admin/change-status')}}/" + table + '/' + status + '/' + id;

            $.ajax({
                url: url ,
                success: function (data) {
                    datatable.draw();
                    toastr.success(data.message)
                    $("#loading").hide();
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procced!',erro)
                }
            });
        });
    </Script>
@endpush