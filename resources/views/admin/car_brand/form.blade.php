<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', '* Brand Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control col-md-4']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group{{ $errors->has('logo') ? ' has-error' : ''}}">
    {!! Form::label('logo', '* Upload Logo: ', ['class' => 'control-label']) !!}
	{!! Form::file('logo', array('class' => 'form-control-file col-md-4 logo','onchange'=>'changeImage(1)')) !!}
	@if(isset($carBrand))
    	<div class="logo-image"><img src="{{ asset('uploads/brand/'.$carBrand->logo) }}" class="changeImage1"></div>
    @endif
    {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
</div>
{!! Form::hidden('old_logo', isset($carBrand->logo)?$carBrand->logo:'') !!}
<div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
    {!! Form::label('status', 'Status: ', ['class' => 'control-label']) !!}
    {!! Form::checkbox('status',1, isset($carBrand->status)?$carBrand->status:1, ['class'=>['checkbox','status']]) !!}
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
