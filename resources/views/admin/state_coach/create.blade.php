@extends('layouts.admin') 
@section('title',"Create State Coach") 
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Create New State Coach</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <!-- button & search bar -->
                    <a href="{{ url('/admin/state-coach') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::open(['url' => '/admin/state-coach', 'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                    @include ('admin.state_coach.form')

                    <div class="form-group">
                        {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection