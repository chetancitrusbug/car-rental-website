@extends('layouts.admin') 
@section('title',"Edit State Coach") 
@push('css')
    <style type="text/css">
        .logo{
            position: relative;
        }.logo-image{
            position: absolute;
            left: 490px;
            top: 250px;
        }img{
            height: 50px;
            width: 50px;
        }
    </style>
@endpush
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h4 class="card-title">Edit State Coach  # {{$stateCoach->title}}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <a href="{{ url('/admin/state-coach') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="card-block">
                {!! Form::model($stateCoach, ['method' => 'PATCH','url' => ['/admin/state-coach', $stateCoach->id],'class' => 'form-horizontal','id' => 'module_form','autocomplete'=>'off']) !!}

                    @include ('admin.state_coach.form')

                    <div class="form-group">
                        {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection