@extends('layouts.frontend')

@section('content')

<div class="container-outer" style="min-height:700px;">
    <!-- start Main Wrapper -->
    @if(isset($termsAndConditions->status) && $termsAndConditions->status == '1')

            <div class="container pt-70 pb-70">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <div class="static-wrapper">
                            @if(isset($termsAndConditions->description) && !empty($termsAndConditions->description))
                                {!! $termsAndConditions->description !!}
                            @else
                                <h2>What are terms and conditions</h2>

                                <p>Terms and Conditions are a set of rules and guidelines that a user must agree to in order to use your website or mobile app.
                                It acts as a legal contract between you (the company) who has the website or mobile app and the user who access your website
                                and mobile app.</p>

                                <p>It’s up to you to set the rules and guidelines that the user must agree to. You can think of your Terms and Conditions agreement
                                as the legal agreement where you maintain your rights to exclude users from your app in the event that they abuse your app,
                                and where you maintain your legal rights against potential app abusers, and so on.</p>

                                <p>Terms and Conditions are also known as Terms of Service or Terms of Use.</p>

                                <p>This type of legal agreement can be used for both your website and your mobile app. It’s not required (it’s not recommended
                                actually) to have separate Terms and Conditions agreements: one for your website and one for your mobile app.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection


