@extends('layouts.frontend')

@section('content')

<div class="container-outer" style="min-height:700px;">
    <!-- start Main Wrapper -->
    @if(isset($privacyPolicy->status) && $privacyPolicy->status == '1')

            <div class="container pt-70 pb-70">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-9">
                        <div class="static-wrapper">
                            @if(isset($privacyPolicy->description) && !empty($privacyPolicy->description))
                                {!! $privacyPolicy->description !!}
                            @else
                                <h2>What is a Privacy Policy</h2>

                                <p>A Privacy Policy is a legal statement that specifies what the business owner does with the personal data collected from users,
                                along with how the data is processed and why.</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>

@endsection


