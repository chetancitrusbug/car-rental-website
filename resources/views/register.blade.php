@extends('layouts.frontend')

@section('content')
<div class="main-wrapper scrollspy-container">
	<div class="breadcrumb-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 hidden-xs">
                    <h2 class="page-title">Register</h2>
                </div>
                <div class="col-xs-12 col-sm-6">

                </div>
            </div>
        </div>
	</div>
	<div class="container-outer pb-20">
        <div class="container">
            <div class="contact-wrapper GridLex-gap-30">
                <div class="GridLex-grid-noGutter-equalHeight">
                    <div class="GridLex-col-6_sm-4_xs-12" style="margin:0 auto;">
                        <div class="contact-form-wrapper-boxed">

                            @foreach ($errors->all() as $error)
                                <div class="error" style="margin-bottom:5px;">* {{ $error }}</div>
                            @endforeach

                            <div class="section-title text-left mb-20">
                                <h4>Register</h4>
                                <p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> These fields are required.</p>
                            </div>
                            <form id="contact-form" method="POST" action="{{ route('registerStore') }}">
                                @csrf
                                <div class="messages"></div>

                                <div class="row gap-20">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('first_name', '* First Name: ', ['class' => 'control-label']) !!}
                                            {!! Form::text('first_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('last_name', '* Last Name: ', ['class' => 'control-label']) !!}
                                            {!! Form::text('last_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('user_name', '* User Name: ', ['class' => 'control-label']) !!}
                                            {!! Form::text('user_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('user_name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('email', '* Email: ', ['class' => 'control-label']) !!}
                                            {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('password', '* Password: ', ['class' => 'control-label']) !!}
                                            {!! Form::password('password',  ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('password_confirmation', '* Password Confirm: ', ['class' => 'control-label']) !!}
                                            {!! Form::password('password_confirmation',  ['class' => 'form-control', 'required' => 'required']) !!}
                                            {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                            {!! Form::label('mobile', '* Mobile: ', ['class' => 'control-label']) !!}
                                            {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
                                            {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <input type="hidden" name="is_register" value="1"/>
                                    <div class="col-sm-12 col-md-12">
                                        <div class="checkbox-block">
                                            <input id="register_accept_checkbox" name="register_accept_checkbox" class="checkbox" value="First Choice" type="checkbox"> By Registering You are accepting the<a href="{{route('terms-and-conditions')}}"> Impressum </a>
                                        </div>
                                        <span class="error terms_check"></span>
                                    </div>

                                    <div class="col-sm-12 col-md-12">
                                        <div class="login-box-box-action">
                                            Already have account? <a href="#" data-toggle="modal" data-target="#login" class="btn-ajax-login" data-toggle="modal">Log-in</a>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12">
                                        <input type="submit" class="btn btn-primary btn-send mt-10" value="Register">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@endsection


