@if(isset($slider->status) && $slider->status == '1')
    @if(isset($slider->image) && !empty($slider->image))
        <div class="hero" style="background-image:url({{url('/uploads/cms/'.$slider->image)}});">
    @else
        <div class="hero" style="background-image:url('front/images/hero-header/03.jpg');">
    @endif
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">

                    @if(isset($slider->description) && !empty($slider->description))
                        {!! $slider->description !!}
                    @else
                        <!-- Hero heading -->
                        <h1 class="animated fadeInUp">Welcome to Autosquare</h1>

                        <!-- Hero subheading -->
                        <p class="animated fadeInUp delay_1">Buy your new car or used car. We have more than a thousand cars for you to choose. The buying process here is so easy to be done.</p>
                    @endif
                    <div class="text-center text-left-xs mt-30">
                        <a href="{{ (isset($slider->description) && !empty($slider->description))?$slider->button_link:route('about-us') }}" class="animated fadeInUp delay_2 btn btn-primary btn-lg">
                            @if(isset($slider->button_title) && !empty($slider->button_title))
                                {{$slider->button_title}}
                            @else
                                Learn More
                            @endif
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="main-search-wrapper">
    <div class="container">
        <div class="text-holder">
            <div class="icon hexagon"> <i class="fa fa-car"></i> </div>
            <div class="text-content"> <span class="uppercase">Looking for Vehicle</span> Find your best vehicle is easy </div>
        </div>
        <form class="form-holder" method="POST" action="{{route('car-list')}}">
            @csrf
            <div class="holder-item mb-20">
                {{Form::select('brand_id',[''=>'Maker']+$brandList,null,['id'=>'car-search-maker','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('model_id',[''=>'Model'],null,['id'=>'car-search-model','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('year',[''=>'Year']+$mfgYear,null,['id'=>'car-search-year','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('price',[''=>'Price']+config('constants.price_option'),request()->get('price',''),['id'=>'car-search-price','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20"> <button type="submit" class="btn btn-block search-button-disp">Search</button> </div>
        </form>
    </div>
</div>