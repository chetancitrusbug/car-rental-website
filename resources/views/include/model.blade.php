<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form class="modal-container" id="loginForm" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
        @csrf

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-center">Log In</h4>
        </div>
        <div class="modal-body">
            <div class="row gap-20">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <span class="login_error error"></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input id="email1" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter your Email Address" type="text" name="email" value="{{old('email')}}">
                        <span class="email_error error"></span>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>Password</label>
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter your password"/>
                        <span class="password_error error"></span>
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="checkbox-block">
                        <input id="remember_me_checkbox" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                    </div>
                </div>

                <div class="col-sm-6 col-md-6">
                    <div class="login-box-link-action">
                        <a href="#" class="forgot_password_link">Forgot password?</a>
                        {{-- <a data-toggle="modal" href="{{route('password.request')}}">Forgot password?</a>  --}}
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="register-box-box-action">
                        No account? <a data-toggle="modal" href="{{route('userRegister')}}" class="registerModal">Register</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal-footer text-left">
            <button type="button" class="btn btn-primary login">Log-in</button>
            <button type="button" data-dismiss="modal" class="btn btn-default modalClose">Close</button>
        </div>
    </form>
</div>

<div class="modal fade" id="forgotPasswordModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <form class="modal-container" id="forgotPasswordForm" method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">
        @csrf

        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-center">Forgot Password</h4>
        </div>
        <div class="modal-body">
            <div class="row gap-20">
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <span class="forgot_password_error error"></span>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>Email</label>
                        <input id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Enter your Email Address" type="text" name="email" value="{{old('email')}}">
                        <span class="email_error error"></span>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal-footer text-left">
            <button type="button" class="btn btn-primary send_pwd_reset_link">{{ __('Send Password Reset Link') }}</button>
        </div>
    </form>
</div>

<div id="mailSendSuccess" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width:495px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close mailSendSuccess_close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Success</h4>
            </div>
            <div class="modal-body">
                <p class="success-content success-content">Mail send successfully.</p>
            </div>
            <div class="modal-footer text-left">
                <button type="button" data-dismiss="modal" class="btn btn-default mailSendSuccess_close">Close</button>
            </div>
        </div>

    </div>
</div>

<div id="bidnow" class="modal border-transparent fade container-sm" tabindex="-1" data-replace="true">
    <form class="modal-container bid-form-submit" method="POST">
        @csrf
        <div class="modal-header">
            <button type="button" class="close bid-close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-center">Bid For This Car</h4>
        </div>
        <div class="modal-body">
            <div class="car-item-sm for-enquiry">
                <div class="image-bg"><img src="" class="bid-image" style="height:105px; width:130px;"/></div>
                <div class="content">
                    <h5 class="bid-title"></h5> {{-- //bid title --}}
                    <div class="bottom">
                        <ul class="car-meta clearfix">
                            <li data-toggle="tooltip bid-tooltip-condition" data-placement="top"> <i class="fa fa-car"></i> <span class='bid-condition'></span></li> {{-- bid condition --}}
                            <li data-toggle="tooltip bid-tooltip-mileage" data-placement="top"> <i class="fa fa-tachometer"></i> <span class='bid-mileage'></span> </li> {{-- bid mileage --}}
                        </ul>
                        <p class="price bid-price"></p>
                    </div>
                </div>
            </div>
            <p>Received overcame oh sensible so at an. Formed do change merely to county it. Am separate contempt domestic to to oh.</p>
            <div class="form-horizontal">
                <div class="form-group row gap-15">
                    <label for="inputEmail1" class="col-sm-12 control-label text-left">Enter Your Latest Bid (&euro;):</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control mb-0" name="price" id="bid-price" placeholder="Enter Your Bid">
                        <input name="car_detail_id" value="" type="hidden" class="car_detail_id"/>
                        <input name="user_id" value="{{Auth::id()}}" type="hidden"/>
                        <input name="car_price" value="" type="hidden"/>
                        <span class="price_error error"></span>
                        <span class="car_detail_id_error error"></span>
                        <span class="user_id_error error"></span>
                        <span class="bid_error error"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer text-left">
            <button type="submit" class="btn btn-sm btn-primary bid-form-submit-button">Bid Now</button>
            <button type="button" class="btn btn-sm btn-warning clear-bid-content" aria-hidden="true">Clear</button>
        </div>
    </form>
</div>

<div id="buynow" class="modal border-transparent fade container-sm" tabindex="-1" data-replace="true">
    <form class="modal-container buy-form-submit" method="POST">
        @csrf
        <div class="modal-header">
            <button type="button" class="close bid-close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title text-center">Bid For This Car</h4>
        </div>
        <div class="modal-body">
            <div class="car-item-sm for-enquiry">
                <p><span class="old_price"></span> is last bid for this car, Are you sure to bid with <span class="new_price"></span>.</p>
                <input name="car_detail_id" value="" type="hidden" class="car_detail_id" />
                <input name="user_id" value="{{Auth::id()}}" type="hidden" />
                <span class="car_detail_id_error error"></span>
                <span class="user_id_error error"></span>
            </div>
        </div>
        <div class="modal-footer text-left">
            <button type="submit" class="btn btn-sm btn-primary buy-form-submit-button">Submit</button>
            <button type="button" class="btn btn-sm btn-warning clear-bid-content" aria-hidden="true">Clear</button>
        </div>
    </form>
</div>

@push('js')
    <script type="text/javascript">
        jQuery(document).ready(function(e) {
            jQuery('.btn-ajax-login').on('click',function(){
                $('.error').html('');
                $('#email').val('');
                $('#password').val('');
            });
            //login user ajax call
            $('.login').on('click',function(e){
                e.preventDefault();
                $('.error').html('');

                $.ajax({
                    type: 'POST',
                    url: "{{ route('userLogin') }}",
                    data: $("#loginForm").serializeArray(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                .done(function(data) {
                    if(data.response == "0"){
                        $('.login_error').html(data.message);
                    }else{
                        location.reload();
                    }
                })
                .fail(function(data) {
                    jQuery.each(data.responseJSON.errors,function(index,val){
                        $('.'+index+'_error').html(val);
                    });
                });
            });

            //forgot password link
            $('.forgot_password_link').on('click',function(event){
                $('#login').hide();
                $('#forgotPasswordModal').modal('show');
            });

            //send password reset link
            $('.send_pwd_reset_link').on('click',function(e){
                e.preventDefault();
                $('.error').html('');

                $.ajax({
                    type: 'POST',
                    url: "{{ route('password.email') }}",
                    data: $("#forgotPasswordForm").serializeArray(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                .done(function(data) {
                    $('.mailSendSuccess_close').attr('data-url',"{{route('/')}}");
                    if(data.response == "0"){
                        $('.forgot_password_error').html(data.message);
                    }else{
                        $('#forgotPasswordModal').hide();
                        $('#mailSendSuccess').modal({ backdrop: 'static', keyboard: false });
                    }
                })
                .fail(function(data) {
                    jQuery.each(data.responseJSON.errors,function(index,val){
                        $('.'+index+'_error').html(val);
                    });
                });
            });

            $('.mailSendSuccess_close').on('click',function(){
                $('#mailSendSuccess').hide();
                if($(this).data('url') == ''){
                    location.reload();
                }else{
                    window.location.href = $(this).data('url');
                }
            });

            $('.buy-form-submit-button').on('click',function(e){
                e.preventDefault();

                $('.buy-form-submit-button, .clear-bid-content').attr('disabled','disabled');
                $("#loading").show();
                $('.error').html('');
                $url = "{{url(request()->path())}}";

                $.ajax({
                    type: 'POST',
                    url: "{{url('car-bid')}}/"+$('.car_detail_id').val(),
                    data: $(".buy-form-submit").serializeArray(),
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })
                .done(function(data) {
                    $("#loading").hide();
                    if(data.response == "0"){
                        $('.bid_error').html(data.message);
                        $('#login').modal('show');
                    }else if(data.response == "1"){
                        $('.bid_error').html(data.message);
                    }else{
                        $('#buynow').hide();
                        $('.mailSendSuccess_close').attr('data-url',$url);
                        $('.success-content').html('Bid done successfully');
                        $('#mailSendSuccess').modal({ backdrop: 'static', keyboard: false });
                    }
                    $('.buy-form-submit-button, .clear-bid-content').removeAttr('disabled');
                })
                .fail(function(data) {
                    $("#loading").hide();
                    jQuery.each(data.responseJSON.errors,function(index,val){
                        $('.'+index+'_error').html(val);
                    });
                    $('.buy-form-submit-button, .clear-bid-content').removeAttr('disabled');
                });
            });

            $('.clear-bid-content').on('click',function(e){
                $('input[name="price"]').val('');
            });
        });

    </script>
@endpush