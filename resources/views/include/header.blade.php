<!-- start Header -->
<header id="header">
    <!-- start Navbar (Header) -->
    <nav class="navbar navbar-primary navbar-fixed-top navbar-sticky-function">
        <div id="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 clearfix">
                        <div class="top-header-widget welcome">
                            @if(Auth::check())
                                @php
                                    $user = Auth::user();
                                @endphp
                                <p>Welcome {{ucfirst($user->first_name)}} {{ucfirst($user->last_name)}} | <a href="{{route('my-account')}}" class="btn-ajax-register" >My Account</a> | <a href="#" class="btn-ajax-register" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log Out</a> </p>
                            @else
                                <p>Welcome Guest | <a href="#" data-toggle="modal" data-target="#login" class="btn-ajax-login" data-toggle="modal">Sign In</a> or <a href="{{route('userRegister')}}" class="btn-ajax-register">Register</a> </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-6 clearfix hidden-xs">
                        <div class="top-header-widget pull-right"> <i class="fa fa-phone"></i> call our agent: <a href="tel:{{$setting['phone']}}">{{$setting['phone']}}</a> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @php
                $title = explode(' ',($setting['site name'])?$setting['site name']:'AUTOSQUARE');
                $first = $title[0];
                $second = implode(' ',array_except($title,['0']));
            @endphp
            <div class="navbar-header"> <a class="navbar-brand" href="{{route('/')}}"><strong class="text-primary">{{$first}}</strong>{{$second}}</a> </div>
            <div id="navbar" class="collapse navbar-collapse navbar-arrow pull-left">

                <!-- SEARCH -->
                <div id="search-container">
                    <form id="search-form" name="search-form" method="get" action="#">
                    <fieldset>
                        <input type="search" name="search" placeholder="Enter your keyword here and then press enter...">
                    </fieldset>
                    </form>
                </div>
                <!-- search-container -->

                <a class="search-button" href="#"></a>
                <ul class="nav navbar-nav" id="responsive-menu">
                    <li> <a href="{{ route('/') }}">Home</a></li>
                    <li> <a href="{{ route('car-list') }}">Car</a></li>
                    <li><a href="{{ route('about-us') }}">About Us</a></li>
                    <li><a href="{{ route('contact') }}">Contact</a></li>
                    {{-- <li class="addcar"><a href="{{ route('add-car') }}">Add Car</a></li> --}}
                </ul>
            </div>
        <!--/.nav-collapse -->

        </div>
        <div id="slicknav-mobile"></div>
    </nav>
    <!-- end Navbar (Header) -->

</header>
{{-- @if (Session::has('flash_success'))
    <div class="alert alert-error alert-success" style="padding: 0 5px; margin-bottom: 0;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('flash_success') }}
    </div>
@endif --}}