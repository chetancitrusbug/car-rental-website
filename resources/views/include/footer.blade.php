<div class="footer-wrapper scrollspy-footer">
	<footer class="secondary-footer">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<p class="copy-right">&#169; Copyright 2018 Autosquare.</p>
				</div>
				<div class="col-sm-6">
					<ul class="secondary-footer-menu clearfix">
                        @if(auth()->check())
                            <li><a href="{{route('my-account')}}">My Account</a></li>
                        @endif
                        <li><a href="{{route('terms-and-conditions')}}">Impressum</a></li>
                        <li><a href="{{route('privacy-policy')}}">Datenschutzerklärung</a></li>
					</ul>
				</div>
			</div>
		</div>
		@include('cookieConsent::index')
	</footer>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	{{ csrf_field() }}
</form>