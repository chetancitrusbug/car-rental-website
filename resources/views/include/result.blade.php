<div class="breadcrumb-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 hidden-xs">
                <h2 class="page-title">{{ $pageTitle }}</h2>
            </div>
            <div class="col-xs-12 col-sm-6">
                <ol class="breadcrumb">
                    <li class="active text">You Are Here:</li>
                    <li><a href="{{ route('/') }}">Home</a></li>
                    <li class="active">{{$location}}</li>
                </ol>
            </div>
        </div>
    </div>
</div>