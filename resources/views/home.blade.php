@extends('layouts.frontend')
@push('css')
    <style>
        .recent_image{
            height: 185px;
            width: 263px;
        }
        .index_brand_image{
            height: 100px;
            width: 100px;
        }
    </style>
@endpush
@section('content')
@if(isset($welcome->status) && $welcome->status == '1')
    <div class="section welcome-wrapper">
        <div class="container">
            <div class="row">

                <div class="col-sm-12 col-md-8">
                    <div class="section-title-2">
                        <h2>
                            @if(isset($welcome->title) && !empty($welcome->title))
                                {!! $welcome->title !!}
                            @else
                                Welcome to Autosquare
                            @endif
                        </h2>
                    </div>
                    @if(isset($welcome->description) && !empty($welcome->description))
                        {!! $welcome->description !!}
                    @else
                        <p>Resembled at perpetual no believing is otherwise sportsman. Is do he dispatched cultivated travelling astonished. Melancholy am considered possession on collecting everything. Doubtful on an juvenile as of servants insisted. Judge why maids led sir whose guest drift her point.</p>
                        <div class="featured-item-wrapper mt-30">
                            <div class="GridLex-gap-30">
                                <div class="GridLex-grid-noGutter-equalHeight">
                                    <div class="GridLex-col-6_sm-6_xs-12_xss-12">
                                        <div class="featured-item">
                                        <div class="icon"> <i class="fa fa-thumbs-up"></i> </div>
                                        <div class="content">
                                            <h5>Trusted By Thousands</h5>
                                            <p>Do in laughter securing smallest sensible no mr hastened.</p>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="GridLex-col-6_sm-6_xs-12_xss-12">
                                        <div class="featured-item">
                                        <div class="icon"> <i class="fa fa-pie-chart"></i> </div>
                                        <div class="content">
                                            <h5>Wide Range Of Vehicles</h5>
                                            <p>As perhaps proceed in in brandon of limited unknown greatly.</p>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="GridLex-col-6_sm-6_xs-12_xss-12">
                                        <div class="featured-item">
                                        <div class="icon"> <i class="fa fa-balance-scale"></i> </div>
                                        <div class="content">
                                            <h5>Professional Dealers</h5>
                                            <p>Distrusts fulfilled happiness unwilling as explained of difficult.</p>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="GridLex-col-6_sm-6_xs-12_xss-12">
                                        <div class="featured-item">
                                        <div class="icon"> <i class="fa fa-money"></i> </div>
                                        <div class="content">
                                            <h5>Faster Buy &amp; Sell</h5>
                                            <p>No landlord of peculiar ladyship attended if contempt ecstatic.</p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @if(isset($welcome->image) && !empty($welcome->image))
                <div class="welcome-image-bg" style="background-image:url('uploads/cms/{{$welcome->image}}');"></div>
            @else
                <div class="welcome-image-bg" style="background-image:url('front/images/welcome/02.jpg');"></div>
            @endif
        </div>
    </div>
@endif

@if(isset($recentCars->status) && $recentCars->status == '1')
    <div class="bg-light section">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                    <h2>
                        @if(isset($recentCars->title) && !empty($recentCars->title))
                            {!! $recentCars->title !!}
                        @else
                            Recently Added Cars
                        @endif
                    </h2>
                    <p>
                        @if(isset($recentCars->sub_title) && !empty($recentCars->sub_title))
                            {!! $recentCars->sub_title !!}
                        @else
                            One sportsman tolerably him extensive put she immediate.
                        @endif
                    </p>
                    </div>
                </div>
            </div>
            <div class="car-item-wrapper">
                <div class="GridLex-gap-30">
                    <div class="GridLex-grid-noGutter-equalHeight GridLex-grid-center">

                        @if (count($recentUpload))
                            @foreach ($recentUpload as $item)
                                <div class="GridLex-col-3_sm-6_xs-6_xss-12">
                                    <div class="car-item"> <a href="{{route('car-show',[$item->id])}}">
                                    <div class="image"> <img src="{{asset('uploads/car_detail/image/'.$item->image)}}" alt="Car" class="recent_image" /> </div>
                                    <div class="content">
                                        <h5>{{$item->brand_name}}</h5>
                                        <h6>{{$item->model_name}}</h6>
                                        <p class="price">&euro;{{number_format($item->price)}}</p>

                                        @if(auth()->check())
                                            <div class="bid-now-link-div"><a class="bid-now-link btn btn-primary" href="{{route('car-show',[$item->id])}}">Bid Now</a></div>
                                        @endif

                                    </div>
                                    <div class="bottom">
                                        <ul class="car-meta clearfix">
                                            <li> <i class="fa fa-cogs"></i> {{ucfirst($item->fuel_type)}} </li>
                                            <li> <i class="fa fa-tachometer"></i> {{$item->engine_size}} </li>
                                        </ul>
                                    </div>
                                    </a> </div>
                                </div>
                            @endforeach
                        @endif

                    </div>
                </div>
            </div>
            <div class="text-center mt-70">
                <a href="{{ (isset($recentCars->description) && !empty($recentCars->description))?$recentCars->button_link:route('car-list') }}" class="btn btn-primary btn-lg">
                    @if(isset($recentCars->button_title) && !empty($recentCars->button_title))
                        {!! $recentCars->button_title !!}
                    @else
                        Browse More Cars
                    @endif
                </a>
            </div>
        </div>
    </div>
@endif

@if(isset($browseBrands->status) && $browseBrands->status == '1')
    <div class="section">
        <div class="container">

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                    <div class="section-title text-center">
                        <div class="section-title">
                            <h2>
                                @if(isset($browseBrands->title) && !empty($browseBrands->title))
                                    {!! $browseBrands->title !!}
                                @else
                                    Browser By Brands
                                @endif
                            </h2>
                            <p>
                                @if(isset($browseBrands->sub_title) && !empty($browseBrands->sub_title))
                                    {!! $browseBrands->sub_title !!}
                                @else
                                    Continuing interested ten stimulated prosperous frequently.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="brand-item-wrapper row gap-0">

                @if(count($brands))
                    @foreach ($brands as $item)
                        <div class="col-xss-4 col-xs-3 col-sm-2 mb-30">
                            <a href="{{url('car-list',[$item->id])}}">
                                @if(file_exists(public_path('uploads/brand/'.$item->logo)))
                                    <img src="{{asset('uploads/brand/'.$item->logo)}}" class="index_brand_image" alt="Car Brand" />
                                @else
                                    <img src="" class="index_brand_image" alt="Car Brand" />
                                @endif
                            </a>
                        </div>
                    @endforeach
                @endif

            </div>

            <div class="text-center mt-40">
                <a href="{{(isset($browseBrands->description) && !empty($browseBrands->description))?$browseBrands->button_link:route('car-list')}}" class="btn btn-primary btn-lg">
                    @if(isset($browseBrands->button_title) && !empty($browseBrands->button_title))
                        {!! $browseBrands->button_title !!}
                    @else
                        Browse More Brands
                    @endif
                </a>
            </div>
        </div>
    </div>
@endif
@endsection






