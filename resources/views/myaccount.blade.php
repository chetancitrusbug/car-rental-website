@extends('layouts.frontend')

@push('css')
    <style type="text/css">
        .detail_image{
            height: 200px;
            width: 295px;
        }
        ul.pagination li {
            float: left;
            margin-left: 3px;
        }

        ul.pagination li a {
            border: 1px solid #CCC !important;
            display: block;
            padding: 5px 15px;
            color: #666;
        }

        ul.pagination li.active a,
        ul.pagination li a:hover {
            border-color: #DB0F38 !important;
            color: #DB0F38 !important;
        }

        ul.pagination li.disabled a,
        ul.pagination li.disabled a:hover,
        ul.pagination li.disabled a:focus {
            color: #666;
            cursor: not-allowed;
            border-color: #CCC;
        }

        .pagination-center ul.pagination {
            text-align: center;
        }

        .pagination-center ul.pagination li {
            float: none;
            display: inline-block;
            margin: 0 2px 0 1px;
        }
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover,.pagination>li:first-child>a, .pagination>li:first-child>span, .pagination>li:last-child>a, .pagination>li:last-child>span {
            z-index: 2;
            color: #DB0F38;
            cursor: default;
            background-color: #FFF;
            border-color: #DB0F38;
            display: block;
            padding: 5px 15px;
            color: #666;
        }
        .pagination>.active>span{
            border: 1px solid #DB0F38 !important;
        }
        .pagination>.disabled>span{
            border: 1px solid #CCC !important;
        }
        .pagination>li>a, .pagination>li>span{
            line-height: 1.8;
        }
        .pagination>li:first-child>a, .pagination>li:first-child>span{
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
        .pagination>li:last-child>a, .pagination>li:last-child>span{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .search_filter {
            background-color: #DB0F38 !important;
        }.container-outer{
            min-height: 750px;
        }
    </style>
@endpush

@section('content')
<div class="container-outer">
    <div class="container">
        <div class="row gap-25-sm">
            <div class="col-sm-8 col-md-9">
                <div class="content-wrapper">
                    <div class="row1">
                        <div class="buy-sell-tab-container clearfix">
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade in active" id="buy">

                                    <h3 class="buy-sell-title">Bid Cars</h3>


                                    <div class="result-sorting-wrapper mb-30">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-5">
                                                @if (count($carDetail))
                                                    <div class="text-holder"> showing {{count($carDetail)}} results </div>
                                                @else
                                                    <div class="text-holder"> No result found </div>
                                                @endif
                                            </div>
                                            <div class="col-sm-12 col-md-7">

                                                <div class="result-sorting-select">
                                                    <form action={{route('my-account')}} method="POST" class="sort_page">
                                                        @csrf {!! Form::select('filter',config('constants.car_detail_sort')+['6'=>'Status'],$filter,['class'=>'custom-select car-sort']) !!}
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="car-item-list-wrapper">
                                        @if(count($carDetail))
                                            @foreach ($carDetail as $item)
                                                <div class="car-item-list">
                                                    <div class="image"> <img src="{{asset('uploads/car_detail/image/'.$item->image)}}" class="detail_image" alt="Car" /> </div>
                                                    <div class="content">
                                                        <h5>{{$item->brand_name}} {{$item->model_name}}</h5>
                                                        <p class="short-info">{{substr($item->description, 0, 120)}}...</p>
                                                        <div class="row">
                                                            <div class="col-xss-12 col-xs-7 col-sm-7 col-md-7"> <span class="price">&euro;{{number_format($item->price)}}</span> <span class="bid-price">/ &euro;{{$item->bid_price}}</span>
                                                            @if(auth()->id() == $item->approve_bid_user_id)
                                                                <span style="color:green; font-weight:bold;">(Approved)</span>
                                                            @elseif($item->approve_bid_user_id > 0)
                                                                <span style="color:red; font-weight:bold;">(Rejected)</span>
                                                            @endif
                                                            </div> 
                                                            <div class="col-xss-12 col-xs-5 col-sm-5 col-md-5">
                                                            <div class="btn-detail-bid-div clearfix">
                                                            <a href="{{url('/car/show/'.$item->id)}}" class="btn btn-primary btn-detail">Details</a> 
                                                            <a href="{{url('/car/bidshow/'.$item->id)}}" class=" btn-bid">More Bid</a>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <ul class="car-meta clearfix">
                                                        <li data-toggle="tooltip" data-placement="top" title="yeas: {{$item->mfg_year}}"> <i class="fa fa-calendar"></i> {{$item->mfg_year}} </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="engine: {{ucfirst($item->fuel_type)}}"> <i class="fa fa-cogs"></i> {{ucfirst($item->fuel_type)}} </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="kilometer: {{$item->mileage}}"> <i class="fa fa-tachometer"></i> {{$item->mileage}} </li>
                                                        <li data-toggle="tooltip" data-placement="top" title="location: {{$item->location}}"> <i class="fa fa-map-marker"></i> {{$item->location}} </li>
                                                    </ul>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="paging-wrapper clearfix">
                                        <div class="pull-right">
                                            {!! $carDetail->render() !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- end of tab 1 -->
                            </div>
                            <!-- end of tab-content -->

                        </div>
                        <!-- end of buy-sell-tab-container -->
                    </div>
                    <!-- end of orw -->


                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <aside class="sidebar-wrapper myaccount-sidebar">


                    <div class="featured-item">
                        <div class="icon"> <i class="fa fa-envelope"></i> </div>
                        <div class="content">
                            <h5>Email</h5>
                            <p><a href="mailto:{{$setting['display email']}}">{{$setting['display email']}}</a></p>
                        </div>
                    </div>

                    <div class="featured-item">
                        <div class="icon"> <i class="fa fa-phone"></i> </div>
                        <div class="content">
                            <h5>Contact Number</h5>
                            <p><a href="tel:{{$setting['phone']}}">{{$setting['phone']}}</a></p>
                        </div>
                    </div>

                    <div class="featured-item">
                        <div class="icon"> <i class="fa fa-pencil"></i> </div>
                        <a href="{{route('user-profile')}}" >
                            <div class="content edit" style="padding-top:10px;">
                                <h5>Edit Profile</h5>
                            </div>
                        </a>
                    </div>

                    <div class="featured-item">
                        <div class="icon"> <i class="fa fa-pencil"></i> </div>
                        <a href="{{route('front-change-password')}}">
                            <div class="content edit" style="padding-top:10px;">
                                <h5>Change password</h5>
                            </div>
                        </a>
                    </div>

            </div>
            </aside>
        </div>
    </div>
</div>
@endsection


