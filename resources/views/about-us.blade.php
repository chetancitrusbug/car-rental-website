@extends('layouts.frontend')

@section('content')
<!-- start Main Wrapper -->
<div class="main-wrapper scrollspy-container">
    @if(isset($aboutus->status) && $aboutus->status == '1')
        <div class="bg-primary section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                        <div class="section-title text-center mb-0">
                            <h2>
                                @if(isset($aboutus->title) && !empty($aboutus->title))
                                    {!! $aboutus->title !!}
                                @else
                                    About Us
                                @endif
                            </h2>
                            <p>
                                @if(isset($aboutus->sub_title) && !empty($aboutus->sub_title))
                                    {!! $aboutus->sub_title !!}
                                @else
                                    Get to know Autosquare a little bit better.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="section">
        <div class="container">
            @if(isset($aboutusMain->status) && $aboutusMain->status == '1')
                <h3 class="heading mt-0">
                    @if(isset($aboutusMain->title) && !empty($aboutusMain->title))
                        {!! $aboutusMain->title !!}
                    @else
                        More Than Car Dealer Websites
                    @endif
                </h3>
                <p>
                    @if(isset($aboutusMain->sub_title) && !empty($aboutusMain->sub_title))
                        {!! $aboutusMain->sub_title !!}
                    @else
                        Unpleasant astonished an diminution up partiality. Noisy an their of meant. Death means up civil do an offer wound of. Called
                        square an in afraid direct. Resolution diminution conviction so mr at unpleasing simplicity no. No it as breakfast up conveying
                        earnestly immediate principle. Him son disposed produced humoured overcame she bachelor improved. Studied however out wishing
                        but inhabit fortune windows.
                    @endif
                </p>
            @endif
            <div class="row">
                <div class="col-sm-6 mt-15">
                    {{-- More Than Car Dealer Websites block --}}
                    @if(isset($aboutusMain->status) && $aboutusMain->status == '1')
                        <div class="image">
                            @if(isset($aboutusMain->image) && !empty($aboutusMain->image))
                                <img src="{!! asset('uploads/cms/'.$aboutusMain->image) !!}" alt="Image /">
                            @else
                                <img src="front/images/about-us/about-us-image-bg-03.jpg" alt="Image /">
                            @endif
                        </div>
                    @endif

                    {{-- Our Mission block --}}
                    @if(isset($aboutusMission->status) && $aboutusMission->status == '1')
                        <h4 class="heading mb-0 uppercase">
                            @if(isset($aboutusMission->title) && !empty($aboutusMission->title))
                                {!! $aboutusMission->title !!}
                            @else
                                Our Mission
                            @endif
                        </h4>
                        @if(isset($aboutusMission->description) && !empty($aboutusMission->description))
                            {!! $aboutusMission->description !!}
                        @else
                            <p>Uncommonly surrounded considered for him are its.</p>
                            <p><strong>1. Boy favourable day can introduced sentiments entreaties.</strong> Noisier carried of in warrant because. So mr plate seems cause chief widen first. Two differed husbands met screened his. Bed was form wife out ask draw. Wholly coming at we no enable. Offending sir delivered questions now new met. Acceptance she interested new boisterous day discretion celebrated.</p>
                            <p><strong>2.Parish so enable innate in formed missed.</strong> Hand two was eat busy fail. Stand smart grave would in so. Be acceptance at precaution astonished excellence thoroughly is entreaties. Who decisively attachment has dispatched. Fruit defer in party me built under first. Forbade him but savings sending ham general. So play do in near park that pain.</p>
                        @endif

                        <div class="image">
                            @if(isset($aboutusMission->image) && !empty($aboutusMission->image))
                                <img src="{!! asset('uploads/cms/'.$aboutusMission->image) !!}" alt="Image /">
                            @else
                                <img src="front/images/about-us/about-us-image-bg-02.jpg" alt="Image /">
                            @endif
                        </div>
                    @endif
                </div>
                <div class="col-sm-6">
                    {{-- Who Are We block --}}
                    @if(isset($whoAreWe->status) && $whoAreWe->status == '1')
                        <h4 class="heading uppercase">
                            @if(isset($whoAreWe->title) && !empty($whoAreWe->title))
                                {!! $whoAreWe->title !!}
                            @else
                                Who Are We
                            @endif
                        </h4>
                        @if(isset($whoAreWe->description) && !empty($whoAreWe->description))
                            {!! $whoAreWe->description !!}
                        @else
                            <p>Boy favourable day can introduced sentiments entreaties. Noisier carried of in warrant because. So mr plate seems cause chief widen first. Two differed husbands met screened his. Bed was form wife out ask draw. Wholly coming at we no enable. Offending sir delivered questions now new met. Acceptance she interested new boisterous day discretion celebrated.</p>
                        @endif
                        <div class="image">
                            @if(isset($whoAreWe->image) && !empty($whoAreWe->image))
                                <img src="{!! asset('uploads/cms/'.$whoAreWe->image) !!}" alt="Image /">
                            @else
                                <img src="front/images/about-us/about-us-image-bg-04.jpg" alt="Image /">
                            @endif
                        </div>
                    @endif
                    {{-- What do we do block --}}
                    @if(isset($whatDoWeDo->status) && $whatDoWeDo->status == '1')
                        <h4 class="heading uppercase">
                            @if(isset($whatDoWeDo->title) && !empty($whatDoWeDo->title))
                                {!! $whatDoWeDo->title !!}
                            @else
                                What do we do
                            @endif
                        </h4>
                        @if(isset($whatDoWeDo->description) && !empty($whatDoWeDo->description))
                            {!! $whatDoWeDo->description !!}
                        @else
                            <p>Parish so enable innate in formed missed. Hand two was eat busy fail. Stand smart grave would in so. Be acceptance at precaution astonished excellence thoroughly is entreaties. Who decisively attachment has dispatched. Fruit defer in party me built under first. Forbade him but savings sending ham general. So play do in near park that pain.</p>
                            <p><strong>1. Parish so enable innate in formed missed.</strong> Hand two was eat busy fail. Stand smart grave would in so. Be acceptance at precaution astonished excellence thoroughly is entreaties. Who decisively attachment has dispatched. Fruit defer in party me built under first. Forbade him but savings sending ham general. So play do in near park that pain.</p>
                            <p><strong>2. Boy favourable day can introduced sentiments entreaties.</strong> Noisier carried of in warrant because. So mr plate seems cause chief widen first. Two differed husbands met screened his. Bed was form wife out ask draw. Wholly coming at we no enable. Offending sir delivered questions now new met. Acceptance she interested new boisterous day discretion celebrated.</p>
                        @endif
                    @endif
                </div>
            </div>
            @if(isset($whyAutoSquare->status) && $whyAutoSquare->status == '1')
                <hr class="mt-100 mb-100">
                <div class="row">
                    <div class="col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                        <div class="section-title text-center">
                            <h2>
                                @if(isset($whyAutoSquare->title) && !empty($whyAutoSquare->title))
                                    {!! $whyAutoSquare->title !!}
                                @else
                                    Why Autosquare
                                @endif
                            </h2>
                            <p>
                                @if(isset($whyAutoSquare->sub_title) && !empty($whyAutoSquare->sub_title))
                                    {!! $whyAutoSquare->sub_title !!}
                                @else
                                    Why Autosquare is the best way to find your next car
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @if(isset($whyAutoSquare->description) && !empty($whyAutoSquare->description))
                        {!! $whyAutoSquare->description !!}
                    @else
                        <div class="col-sm-4">
                            <div class="featured-header-most-top">
                                <h5><span>Buy</span></h5>
                                <div class="icon"><i class="fa fa-shopping-cart"></i></div>
                                <p>Unpleasant astonished an diminution up partiality. Noisy an their of meant bachelor improved. Studied however out
                                    fortune windows.</p>
                                <div class="btn btn-primary btn-div"><a href="#">Get Your Car</a></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="featured-header-most-top">
                                <h5><span>Build &amp; Price</span></h5>
                                <div class="icon"><i class="fa fa-cogs"></i></div>
                                <p>Death means up civil do an offer wound of. Called square an in afraid direct. Him son disposed produced humoured
                                    overcame she.</p>
                                <div class="btn btn-primary btn-div"><a href="#">Build</a></div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="featured-header-most-top">
                                <h5><span>Sell</span></h5>
                                <div class="icon"><i class="fa fa-car"></i></div>
                                <p>Resolution diminution conviction so mr at unpleasing simplicity no. No it as breakfast up conveying earnestly immediate
                                    principle.</p>
                                <div class="btn btn-primary btn-div"><a href="#">Sell Your Car</a></div>
                            </div>
                        </div>
                    @endif
                </div>
            @endif
        </div>
    </div>
</div>
@endsection


