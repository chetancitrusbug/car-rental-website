<!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
<div data-active-color="white" data-background-color="man-of-steel" class="app-sidebar">
    <!-- main menu header-->
    <!-- Sidebar Header starts-->
    <div class="sidebar-header">
        <div class="logo clearfix">
            <div class="logo-img">
                @php
                    $title = explode(' ',($setting['site name'])?$setting['site name']:'AUTO SQUARE');
                    $first = $title[0];
                    $second = implode(' ',array_except($title,['0']));
                @endphp
                <a href="{{ url('/') }}" class="logo-text float-left">
                    <strong class="text-primary">{{$first}}</strong>{{$second}}
                </a>
            </div>
            <a id="sidebarClose" href="javascript:;" class="nav-close d-block d-md-block d-lg-none d-xl-none"><i class="ft-x"></i></a>
        </div>
    </div>
    <!-- Sidebar Header Ends-->
    <!-- / main menu header-->
    <!-- main menu content-->
    <div class="sidebar-content">
        <div class="nav-container">
            <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
                <li class="nav-item">
                    <a href="{{ url('admin/users') }}"><i class="ft-user"></i><span data-i18n="" class="menu-title">Users</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/car-brand') }}"><i class="ft-package"></i><span data-i18n="" class="menu-title">Car Brand</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/car-model') }}"><i class="ft-copy"></i><span data-i18n="" class="menu-title">Car Model</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/car-detail') }}"><i class="fa fa-car"></i><span data-i18n="" class="menu-title">Car Detail</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/state-coach') }}"><i class="ft-layers"></i><span data-i18n="" class="menu-title">State Coach</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/abgasnorm') }}"><i class="ft-crosshair"></i><span data-i18n="" class="menu-title">Abgasnorm</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/transmition') }}"><i class="ft-type"></i><span data-i18n="" class="menu-title">Transmition</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/feature') }}"><i class="ft-zap"></i><span data-i18n="" class="menu-title">Feature</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/fuel-type') }}"><i class="fa fa-fire"></i><span data-i18n="" class="menu-title">Fuel Type</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/contact') }}"><i class="fa fa-list-alt"></i><span data-i18n="" class="menu-title">Contact</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/setting') }}"><i class="fa fa-cog"></i><span data-i18n="" class="menu-title">Setting</span></a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('admin/cms') }}"><i class="fa fa-home"></i><span data-i18n="" class="menu-title">CMS</span></a>
                </li>
            </ul>
        </div>
    </div>
    <!-- main menu content-->
    <div class="sidebar-background"></div>
    <!-- main menu footer-->
    <!-- include includes/menu-footer-->
    <!-- main menu footer-->
</div>