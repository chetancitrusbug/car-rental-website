<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title Of Site -->
	<title>@yield('title') {{ ($setting['site name'])?$setting['site name']:config('app.name') }}</title>
	<meta name="description" content="Autosquare for Car Deal" />
	<meta name="keywords" content="car dealer, automotive, car auction, car shop, showroom" />
	<meta name="author" content="Autosquare">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('front/images/ico/apple-touch-icon-144-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('front/images/ico/apple-touch-icon-114-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('front/images/ico/apple-touch-icon-72-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" href="{{ asset('front/images/ico/apple-touch-icon-57-precomposed.png') }}">
	<link rel="shortcut icon" href="{{ asset('front/images/ico/favicon.png') }}">

	<!-- CSS Cores and Plugins -->
	<link rel="stylesheet" type="text/css" href="{{ asset('front/bootstrap/css/bootstrap.min.css') }}" media="screen">
	<link href="{{ asset('front/css/animate.css') }}" rel="stylesheet">
	<link href="{{ asset('front/css/main.css') }}" rel="stylesheet">
	<link href="{{ asset('front/css/plugin.css') }}" rel="stylesheet">

	<!-- CSS Custom -->
	<link href="{{ asset('front/css/style.css') }}" rel="stylesheet">

	<!-- Your Own Style -->
	<link href="{{ asset('front/css/your-style.css') }}" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

	@stack('css')
</head>

<body class="with-top-header">
    <div id="loading">
        <img id="loading-image" src="{{asset('uploads/loading.gif')}}" alt="Loading..." />
    </div>
	<!-- start Container Wrapper -->
	<div class="container-wrapper">

		@include('include.header')

		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">

			@php
				$route = \Route::currentRouteName();
			@endphp

			@if($route == '/')
				<!-- start hero-header -->
				@include('include.slider')
			@elseif(in_array($route,['car-list','contact','add-car','car-show','my-account','terms-and-conditions','privacy-policy']))
				@include('include.result')
			@endif

			@yield('content')
		</div>

  		@include('include.footer')
  <!-- end footer-wrapper -->

	</div>
<!-- end Container Wrapper -->

	<!-- start Back To Top -->
	<div id="back-to-top"> <a href="#"><i class="ion-ios-arrow-up"></i></a> </div>
	<!-- end Back To Top -->

	@include('include.model')

	<!-- JS Global -->
	<script type="text/javascript" src="{{ asset('front/js/jquery-2.2.4.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/jquery-migrate-1.4.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/bootstrap/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/jquery.waypoints.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/jquery.easing.1.3.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/SmoothScroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/jquery.slicknav.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/jquery.placeholder.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/fancySelect.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/slick.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/bootstrap-modalmanager.js') }}"></script>
	<script type="text/javascript" src="{{ asset('front/js/bootstrap-modal.js') }}"></script>
    <script type="text/javascript" src="{{ asset('front/js/customs.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            loadModel();
            @if(auth()->check())
            @else
                $("#loading").hide();
            @endif

            jQuery(document).ready(function(){
                setTimeout(function(){
                    jQuery('.alert-error').hide();
                }, 3000);
            });
        });
        $('#car-search-maker').fancySelect().on('change.fs', function (event) {
                loadModel();
        });

        function loadModel(){
            $.ajax({
                url: "{{url('car-model-front')}}",
                data: { brand_id: $('#car-search-maker').val() },
            })
            .done(function (data) {
                $('select[name="model_id"] option:not(:first)').remove();
                if(data != ''){
                    $.each(data, function(index, val) {
                        $('select[name="model_id"]').append("<option value='"+index+"'>"+val+"</option>");
                    });
                }
                $('select[name="model_id"]').val("{{request()->get('model_id','')}}");
                $('select[name="model_id"]').trigger('update.fs');
            });
        }

        $('.bid_button').on('click',function(){
            @if(!auth()->check())
                $('#login').modal('show');
                return false;
            @endif
            $('input[name="price"]').val('');
            $('.bid-form-submit').attr('action',"{{url('car-bid')}}/"+$(this).data('id'));
            $('.car_detail_id').val($(this).data('id'));
            var url = "{{url('/uploads/car_detail/image')}}";
            $('.bid-image').attr('src',url+"/"+$(this).data('image'));
            $('.bid-title').html($(this).data('brand')+' '+$(this).data('model'));
            $('.bid-condition').html($(this).data('condition'));
            $('.bid-tooltip-condition').attr('title', 'condition : '+$(this).data('condition'));
            $('.bid-mileage').html($(this).data('mileage'));
            $('.bid-tooltip-mileage').attr('title', 'condition : '+$(this).data('condition'));
            $('.bid-price').html('&euro;'+$(this).data('price'));
            $('input[name="car_price"]').val($(this).data('price2'));
            // debugger;
            $('.old_price').html('&euro;'+$(this).data('price'));
            $('.new_price').html('&euro;'+$(this).data('bid_price'));
            // $('#bidnow').modal({ backdrop: 'static', keyboard: false });
            $('#buynow').modal({ backdrop: 'static', keyboard: false });
        });

        $('#loading-image').bind('ajaxStart', function(){
            $("#loading").show();
        }).bind('ajaxStop', function(){
            $("#loading").hide();
        });

        //sort by car list
        $('.car-sort').fancySelect().on('change.fs', function (event) {
            $(".sort_page").submit();
        });

        @if (Session::has('flash_success'))
            $('.mailSendSuccess_close').attr('data-url',"");
            $('.success-content').html("{{ Session::get('flash_success') }}");
            $('#mailSendSuccess').modal({ backdrop: 'static', keyboard: false });
        @endif
    </script>
	@stack('js')
</body>
</html>