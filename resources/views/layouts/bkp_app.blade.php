<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->
<head>
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>@yield('page-title') {{ config('app.name') }}</title>

<meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
<meta content='yes' name='apple-mobile-web-app-capable'>
<meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
<link href='{{ asset('frontend/images/favicon.png') }}' rel='shortcut icon'>
<link href='{{ asset('frontend/images/favicon.ico') }}' rel='icon' type='image/ico'>

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>

<link href="{{ asset('frontend/css/style.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/bootstrap.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/font-awesome.min.css') }}" media="all" rel="stylesheet" type="text/css" />

<link href="{{ asset('frontend/css/streamline-small.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('frontend/css/streamline-large.css') }}" media="all" rel="stylesheet" type="text/css" />
@stack('css')
<script src="{{ asset('frontend/js/modernizr.js') }}" type="text/javascript"></script>
    <!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->

</head>
<body>

<div id="wrapper">
    <div class="top-header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="logo-div">
                        <div class="logo-box">
                            <span class="logo-txt">
                                <a href="{{ url('/') }}" class="logo-text float-left">
                                    <strong class="text-primary">AUTO</strong>SQUARE
                                </a>
                            </span>
                        </div>
                    </div><!-- end of logo div -->
                </div>
            </div>
        </div>
    </div><!-- end of top header -->
    <div class="login-regi-div clearfix">
                
        <div class="logo-box">
            <span class="logo-txt"><a href="{{ url('/') }}"><img src="{{ asset('frontend/images/logo.png') }}" class="img-responsive" alt=""></a></span>
        </div>

        @include('include.flash-message')

        @yield('content')

    </div><!-- end of login-regi-div -->

<p class="log-reg-footer-txt">&copy; {{ config('app.name') }}</p>

</div><!-- end of wrapper -->

<script src="{{ asset('frontend/js/jquery.min.js') }}" type="text/javascript"></script> 
<script src="{{ asset('frontend/js/bootstrap.min.js') }}" type="text/javascript"></script>
<link href="{{ asset('frontend/css/hover-dropdown-menu.css') }}" rel="stylesheet" />
<script type="text/javascript" src="{{ asset('frontend/js/hover-dropdown-menu.js') }}"></script>
<!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="{{ asset('frontend/js/jquery.hover-dropdown-menu-addon.js') }}"></script> 

<script type="text/javascript" src="{{ asset('frontend/js/home-roundbox-hover.js') }}"></script> 

<script type="text/javascript" src="{{ asset('frontend/js/custom.js') }}"></script> 
<script src='https://www.google.com/recaptcha/api.js'></script>
@stack('js')
</body>
</html>

