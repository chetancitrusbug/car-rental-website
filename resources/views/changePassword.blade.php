@extends('layouts.frontend')
@push('name')
    <style type="text/css">
        .container-outer{  }
    </style>
@endpush
@section('content')
<div class="container-outer pb-20" style="min-height: 770px;">
    <div class="container">
        <div class="contact-wrapper GridLex-gap-30">
            <div class="GridLex-grid-noGutter-equalHeight">
                <div class="GridLex-col-6_sm-4_xs-12" style="margin:0 auto;">
                    <div class="contact-form-wrapper-boxed">
                        <div class="section-title text-left mb-20">
                            <h4>Change Password</h4>
                            <p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> These fields are required.</p>
                        </div>
                        @if (Session::has('flash_warning'))
                        <div class="alert alert-error alert-warning" style="padding: 0 5px; margin-bottom: 0;">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_warning')
                            }}
                        </div>
                        @endif
                        <form id="contact-form" method="post" action="{{route('front-password-update')}}">
                            @csrf
                            <div class="messages"></div>

                            <div class="row gap-20">
                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>*Old Password</label>
                                        <input class="form-control" placeholder="Old password" type="password" name="current_password" value="{{old('current_password')}}">
                                        {!! $errors->first('current_password', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>*New Password</label>
                                        <input class="form-control" placeholder="New password" type="password" name="password" value="{{old('password')}}">
                                        {!! $errors->first('password', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>*Confirm New Password</label>
                                        <input class="form-control" placeholder="Confirm new password" type="password" name="password_confirmation" value="{{old('password_confirmation')}}">
                                        {!! $errors->first('password_confirmation', '<p class="help-block error">:message</p>') !!}
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12">
                                    <input type="hidden" name="is_register" value="1">
                                    <input type="submit" class="btn btn-primary btn-send mt-10" value="Update">
                                    <a type="button" class="btn btn-warning btn-send mt-10" href="{{route('my-account')}}">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection