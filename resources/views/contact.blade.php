@extends('layouts.frontend')

@section('content')
<div class="container-outer pb-20">
    <div class="container">
        <div class="contact-wrapper GridLex-gap-30">
            <div class="GridLex-grid-noGutter-equalHeight">
                <div class="GridLex-col-6_sm-4_xs-12" style="margin:0 auto;">
                    <div class="contact-form-wrapper-boxed">
                        <div class="section-title text-left mb-20">
                            <h4>Contact us for help</h4>
                            <p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> These fields are required.</p>
                        </div>
                        <form id="contact-form" method="post" action="{{route('contact-submit')}}">
                            @csrf
                            <div class="messages"></div>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="contact_name">*Your Name <span class="font10 text-danger">(required)</span></label>
                                            <input id="contact_name" type="text" name="name" value="{{old('name')}}" class="form-control" placeholder="Please enter your full name"  data-error="Firstname is required.">
                                            {!! $errors->first('name', '<p class="help-block error">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="form_email">*Your Email <span class="font10 text-danger">(required)</span></label>
                                            <input id="form_email" type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="Please enter your email"  data-error="Valid email is required.">
                                            {!! $errors->first('email', '<p class="help-block error">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Subject</label>
                                            <input id="form_lastname" type="text" name="title" value="{{old('title')}}" class="form-control" placeholder="Please enter your Subject *"  data-error="Subject is required.">
                                            {!! $errors->first('title', '<p class="help-block error">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="form_message">*Message <span class="font10 text-danger">(required)</span></label>
                                            <textarea id="form_message" name="message" class="form-control" placeholder="Message for me" rows="8"  data-error="Please,leave us a message.">{{old('message')}}</textarea>
                                            {!! $errors->first('message', '<p class="help-block error">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12">
                                        <input type="submit" class="btn btn-primary btn-send mt-10" value="Send message">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection