@extends('layouts.frontend')
@push('css')
    <style type="text/css">
        .detail_image{
            height: 200px;
            width: 295px;
        }
        ul.pagination li {
            float: left;
            margin-left: 3px;
        }

        ul.pagination li a {
            border: 1px solid #CCC !important;
            display: block;
            padding: 5px 15px;
            color: #666;
        }

        ul.pagination li.active a,
        ul.pagination li a:hover {
            border-color: #DB0F38 !important;
            color: #DB0F38 !important;
        }

        ul.pagination li.disabled a,
        ul.pagination li.disabled a:hover,
        ul.pagination li.disabled a:focus {
            color: #666;
            cursor: not-allowed;
            border-color: #CCC;
        }

        .pagination-center ul.pagination {
            text-align: center;
        }

        .pagination-center ul.pagination li {
            float: none;
            display: inline-block;
            margin: 0 2px 0 1px;
        }
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover,.pagination>li:first-child>a, .pagination>li:first-child>span, .pagination>li:last-child>a, .pagination>li:last-child>span {
            z-index: 2;
            color: #DB0F38;
            cursor: default;
            background-color: #FFF;
            border-color: #DB0F38;
            display: block;
            padding: 5px 15px;
            color: #666;
        }
        .pagination>.active>span{
            border: 1px solid #DB0F38 !important;
        }
        .pagination>.disabled>span{
            border: 1px solid #CCC !important;
        }
        .pagination>li>a, .pagination>li>span{
            line-height: 1.8;
        }
        .pagination>li:first-child>a, .pagination>li:first-child>span{
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
        .pagination>li:last-child>a, .pagination>li:last-child>span{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .search_filter {
            background-color: #DB0F38 !important;
        }
    </style>
@endpush
@section('content')

<div class="container-outer">
    <div class="container">
        <div class="row gap-25-sm">
            <div class="col-sm-8 col-md-9">
                <div class="content-wrapper">
                    <div class="result-sorting-wrapper mb-30">
                        <div class="row">
                            
                        </div>
                    </div>
                    <div class="car-item-list-wrapper">
                        @if(count($carBids))
                            @foreach ($carBids as $item)
                                <?php /* echo '<pre>';print_r($item); echo '</pre>'; */?>
                                <div class="car-item-list">
                                    
                                    <div class="content">
                                        <h5>&euro;{{number_format($item->price)}}</h5>
                                        <p class="short-info">{{$item->created_at}}</p>
                                        
                                    </div>
                                    
                                </div>
                            @endforeach
                        @endif 
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <aside class="sidebar-wrapper">
                    <div class="mt-40 bt pt-20">
                        <div class="featured-item">
                            <div class="icon"> <i class="fa fa-phone"></i> </div>
                            <div class="content">
                            <h5>Quick Call</h5>
                            <p><a href="tel:66 74 554 884">+66 74 554 884</a></p>
                            </div>
                        </div>
                        <div class="featured-item">
                            <div class="icon"> <i class="fa fa-envelope"></i> </div>
                            <div class="content">
                            <h5>Quick Email</h5>
                            <p><a href="mailto:braindog@gmail.com">support@autosquare.com</a></p>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
@endsection
