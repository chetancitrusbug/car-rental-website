@extends('layouts.frontend')

@section('content')
    <div class="container-outer pb-20">
		<div class="container">
			<div class="contact-wrapper GridLex-gap-30">
                <div class="GridLex-grid-noGutter-equalHeight">
                    <div class="GridLex-col-12_sm-12_xs-12">
                        <div class="contact-form-wrapper-boxed add-car-form">
                            <div class="section-title text-left mb-20">
                                <h4>Add Your Car</h4>
                                <p>Expression acceptance imprudence particular had eat unsatiable.</p>
                            </div>
                            <form  method="post" action="#">
                                @csrf
                                @include('car.form')
                            </form>
                        </div>
                    </div>
                </div>
			</div>
		</div>
    </div>
@endsection


