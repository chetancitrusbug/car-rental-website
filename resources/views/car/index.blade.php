@extends('layouts.frontend')
@push('css')
    <style type="text/css">
        .detail_image{
            height: 200px;
            width: 295px;
        }
        ul.pagination li {
            float: left;
            margin-left: 3px;
        }

        ul.pagination li a {
            border: 1px solid #CCC !important;
            display: block;
            padding: 5px 15px;
            color: #666;
        }

        ul.pagination li.active a,
        ul.pagination li a:hover {
            border-color: #DB0F38 !important;
            color: #DB0F38 !important;
        }

        ul.pagination li.disabled a,
        ul.pagination li.disabled a:hover,
        ul.pagination li.disabled a:focus {
            color: #666;
            cursor: not-allowed;
            border-color: #CCC;
        }

        .pagination-center ul.pagination {
            text-align: center;
        }

        .pagination-center ul.pagination li {
            float: none;
            display: inline-block;
            margin: 0 2px 0 1px;
        }
        .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover,.pagination>li:first-child>a, .pagination>li:first-child>span, .pagination>li:last-child>a, .pagination>li:last-child>span {
            z-index: 2;
            color: #DB0F38;
            cursor: default;
            background-color: #FFF;
            border-color: #DB0F38;
            display: block;
            padding: 5px 15px;
            color: #666;
        }
        .pagination>.active>span{
            border: 1px solid #DB0F38 !important;
        }
        .pagination>.disabled>span{
            border: 1px solid #CCC !important;
        }
        .pagination>li>a, .pagination>li>span{
            line-height: 1.8;
        }
        .pagination>li:first-child>a, .pagination>li:first-child>span{
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
        }
        .pagination>li:last-child>a, .pagination>li:last-child>span{
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
        }
        .search_filter {
            background-color: #DB0F38 !important;
        }
    </style>
@endpush
@section('content')

<div class="container-outer">
    <div class="container">
        <div class="row gap-25-sm">
            <div class="col-sm-8 col-md-9">
                <div class="content-wrapper">
                    <div class="result-sorting-wrapper mb-30">
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                @if (count($carDetail))
                                    <div class="text-holder"> showing {{count($carDetail)}} results </div>
                                @else
                                    <div class="text-holder"> No result found </div>
                                @endif
                            </div>
                            <div class="col-sm-12 col-md-7">
                                <div class="result-sorting-select">
                                    <form action={{route('car-list')}} method="POST" class="sort_page">
                                        @csrf
                                        {!! Form::select('filter',config('constants.car_detail_sort'),$filter,['class'=>'custom-select car-sort']) !!}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="car-item-list-wrapper">
                        @if(count($carDetail))
                            @foreach ($carDetail as $item)
                                <div class="car-item-list">
                                    <div class="image"> <img src="{{asset('uploads/car_detail/image/'.$item->image)}}" class="detail_image" alt="Car" /> </div>
                                    <div class="content">
                                        <h5>{{$item->brand_name}} {{$item->model_name}}</h5>
                                        <p class="short-info">{{substr($item->description, 0, 120)}}...</p>
                                        <div class="row">
                                            <div class="col-xss-12 col-xs-6 col-sm-6 col-md-6"> <span class="price">&euro;{{number_format($item->price)}}</span> </div>
                                            <div class="col-xss-12 col-xs-3 col-sm-3 col-md-3">

                                                    <div class="">
                                                        <a href="#" data-image="{{$item->image}}" data-brand="{{$item->brand_name}}" data-model="{{$item->model_name}}" data-condition="{{$item->condition}}" data-mileage="{{$item->mileage}}" data-price="{{number_format($item->price)}}" data-bid_price="{{number_format($item->price+100)}}" data-price2="{{$item->price}}" data-id="{{$item->id}}" class="bid-now-link bid_button btn btn-primary">Buy</a>
                                                    </div>

                                            </div>
                                            <div class="col-xss-12 col-xs-3 col-sm-3 col-md-3"> <a href="{{url('/car/show/'.$item->id)}}" class="btn btn-primary">Details</a> </div>
                                        </div>
                                    </div>
                                    <ul class="car-meta clearfix">
                                        <li data-toggle="tooltip" data-placement="top" title="yeas: {{$item->mfg_year}}"> <i class="fa fa-calendar"></i> {{$item->mfg_year}} </li>
                                        <li data-toggle="tooltip" data-placement="top" title="engine: {{ucfirst($item->fuel_type)}}"> <i class="fa fa-cogs"></i> {{ucfirst($item->fuel_type)}} </li>
                                        <li data-toggle="tooltip" data-placement="top" title="kilometer: {{$item->mileage}}"> <i class="fa fa-tachometer"></i> {{$item->mileage}} </li>
                                        <li data-toggle="tooltip" data-placement="top" title="location: {{$item->location}}"> <i class="fa fa-map-marker"></i> {{$item->location}} </li>
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="paging-wrapper clearfix">
                        <div class="pull-right">
                            {!! $carDetail->render() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-md-3">
                <aside class="sidebar-wrapper">
                    @include('car.search')

                    <div class="mt-40 bt pt-20">
                        <div class="featured-item">
                            <div class="icon"> <i class="fa fa-phone"></i> </div>
                            <div class="content">
                            <h5>Quick Call</h5>
                            <p><a href="tel:{{$setting['phone']}}">{{$setting['phone']}}</a></p>
                            </div>
                        </div>
                        <div class="featured-item">
                            <div class="icon"> <i class="fa fa-envelope"></i> </div>
                            <div class="content">
                            <h5>Quick Email</h5>
                            <p><a href="mailto:{{$setting['display email']}}">{{$setting['display email']}}</a></p>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>
</div>
@endsection
