@extends('layouts.frontend')

@section('content')
    <div class="container-outer pb-20">
		<div class="container">
			<div class="contact-wrapper GridLex-gap-30">
			<div class="GridLex-grid-noGutter-equalHeight">
			
				<div class="GridLex-col-12_sm-12_xs-12">
				<div class="contact-form-wrapper-boxed add-car-form">
					<div class="section-title text-left mb-20">
					<h4>Add Your Car</h4>
					<p>Expression acceptance imprudence particular had eat unsatiable.</p>
					</div>
					<form  method="post" action="#">
						@csrf
					<div class="row gap-20">
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Make</label>
									<select class="form-control"><option>Select Make</option>
									<option>Honda</option><option>Hyundai</option>
									<option>BMW</option>
									<option>AUDI</option>
									</select>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Model</label>
									<select class="form-control"><option>Select Model</option>
									<option>Civic</option>
									<option>Jazz</option>
									<option>Amaze</option>
									<option>Verna</option>
									<option>i20</option>
									<option>s320</option>
									</select>
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Type</label>
									<input class="form-control" placeholder="Enter Type" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Year</label>
									<input class="form-control" placeholder="Enter Year" type="text"> 
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Fuel Type</label>
									<select class="form-control"><option>Select Fuel Type</option>
									<option>Unleaded</option>
									<option>Petrol</option>
									<option>Diesel</option>
									<option>Electrical</option>
									<option>Hybrid</option>
									<option>CNG</option>
									<option>Auto Gas</option>
									</select>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Abgasnorm</label>
									<select class="form-control"><option>Select Abgasnorm</option>
										<option>Euro 1</option>
										<option>Euro 2</option>
										<option>Euro 3</option>
										<option>Euro 4</option>
										<option>Euro 5</option>
										<option>Euro 6</option>
									</select>
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Engine Size</label>
									<input class="form-control" placeholder="Enter Engine Size" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Engine Power</label>
									<input class="form-control" placeholder="Enter Engine Power" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>CO2 Emission</label>
									<input class="form-control" placeholder="Enter CO2 Emission" type="text"> 
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Transmission</label>
									<select class="form-control"><option>Select Transmission</option>
										<option>Auto</option>
										<option>Manual</option>
										<option>Half Auto</option>
									</select>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Mileage</label>
									<input class="form-control" placeholder="Enter Mileage" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4 radio-check-div">						
								<div class="form-group"> 
									<label>All Wheel Drive </label>
									<p><span><input name="yes" type="radio" value="1" /> Yes</span> <span><input name="no" type="radio" value="" /> No</span></p>
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Number of gears </label>
									<select class="form-control"><option>Select Number of gears</option>
										<option>5</option>
										<option>6</option>
										<option>7</option>
										<option>8</option>
										<option>9</option>
									</select>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>State Coach</label>
									<select class="form-control"><option>Select State Coach</option>
										<option>Limousine</option>
										<option>Combi</option>
										<option>Sedan</option>
										<option>Coupe</option>
										<option>Cabrio</option>
										<option>SUV</option>
										<option>others</option>
									</select>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Exterior </label>
									<input class="form-control" placeholder="Enter Exterior" type="text"> 
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Interior</label>
									<input class="form-control" placeholder="Enter Interior" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Condition</label>
									<input class="form-control" placeholder="Enter Condition" type="text"> 
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Price</label>
									<input class="form-control" placeholder="Enter Engine Size" type="text"> 
								</div>						
							</div>
							
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Damage</label>
									<textarea class="form-control" placeholder="Enter Damage"></textarea>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									<label>Description</label>
									<textarea class="form-control" placeholder="Enter Description"></textarea>
								</div>						
							</div>
							<div class="col-sm-4 col-md-4">						
								<div class="form-group"> 
									
								</div>						
							</div>
							
							
							<div class="col-sm-12 col-md-12">						
								<div class="form-group"> 
									<label>Images</label>
									<div class="col-lg-12_sm-12_xs-12">
										<input type="file" class="file-input-ajax" multiple="multiple">
										
									</div>
									
								</div>						
							</div>
							
							
							<div class="col-sm-12 col-md-12">						
								<div class="form-group"> 
									<label>Car Features</label>
									<div class="col-lg-12_sm-12_xs-12">
										<ul class="car-feature-ul clearfix">
											<li><input name="yes" type="checkbox" value="1" /> Navigation</li>
											<li><input name="yes" type="checkbox" value="1" /> Autopilot</li>
											<li><input name="yes" type="checkbox" value="1" /> Aircondition manual</li>
											<li><input name="yes" type="checkbox" value="1" /> Aircondition Automatic</li>
											<li><input name="yes" type="checkbox" value="1" /> SECTION</li>
											<li><input name="yes" type="checkbox" value="1" /> Lane Departure</li>
											<li><input name="yes" type="checkbox" value="1" /> USB</li>
											<li><input name="yes" type="checkbox" value="1" /> AUX, LED, Start & Stop, LED Daylight, </li>
											<li><input name="yes" type="checkbox" value="1" /> All wheel drive</li>
											<li><input name="yes" type="checkbox" value="1" /> Board Computer </li>
											<li><input name="yes" type="checkbox" value="1" /> Backup Camera </li>
											<li><input name="yes" type="checkbox" value="1" /> CD player</li>
											<li><input name="yes" type="checkbox" value="1" /> Roof Rails</li>
											<li><input name="yes" type="checkbox" value="1" /> Electric windows</li>
											<li><input name="yes" type="checkbox" value="1" /> Electric sidemirror</li>
											<li><input name="yes" type="checkbox" value="1" /> Electric immobiliser</li>
											<li><input name="yes" type="checkbox" value="1" /> ESP</li>
											<li><input name="yes" type="checkbox" value="1" /> Isofix</li>
											<li><input name="yes" type="checkbox" value="1" /> leather steering wheel</li>
											<li><input name="yes" type="checkbox" value="1" /> Alloy wheels</li>
											<li><input name="yes" type="checkbox" value="1" /> Light sensor</li>
											<li><input name="yes" type="checkbox" value="1" /> Multifunctional steering wheel</li>
											<li><input name="yes" type="checkbox" value="1" /> Fog lights</li>
											<li><input name="yes" type="checkbox" value="1" /> Particulate Filter</li>
											<li><input name="yes" type="checkbox" value="1" /> Rain sensor</li>
											<li><input name="yes" type="checkbox" value="1" /> Maintenance Guide</li>
											<li><input name="yes" type="checkbox" value="1" /> Power steering</li>
											<li><input name="yes" type="checkbox" value="1" /> Summer tires</li>
											<li><input name="yes" type="checkbox" value="1" /> Daytime running lights</li>
											<li><input name="yes" type="checkbox" value="1" /> Cruise control</li>
											<li><input name="yes" type="checkbox" value="1" /> Tuner / Radio</li>
											<li><input name="yes" type="checkbox" value="1" /> Winter tires</li>
											<li><input name="yes" type="checkbox" value="1" /> Central Locking System</li>
										</ul>									
									</div>
									
								</div>						
							</div>
							<p>&nbsp;</p>
							
							
							<div class="col-xs-12 col-sm-12">
							<input type="submit" class="btn btn-primary btn-send mt-10" value="Submit">
						</div>
						<div class="col-md-12">
							<p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> These fields are required.</p>
						</div>
						</div>
					</form>
				</div>
				</div>
			</div>
			</div>
		</div>
    </div>
@endsection


