<div class="result-filter-wrapper">
    <h3>Filter Your Search</h3>
    <div class="content">
        <form class="form-holder" method="POST" action="{{route('car-list')}}">
            @csrf
            <div class="holder-item mb-20">
                {{Form::select('brand_id',[''=>'Maker']+$brandList,request()->get('brand_id',$brandId),['id'=>'car-search-maker','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('model_id',[''=>'Model'],request()->get('model_id',''),['id'=>'car-search-model','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('year',[''=>'Year']+$mfgYear,request()->get('year',''),['id'=>'car-search-year','class'=>'custom-select'])}}
            </div>
            <div class="holder-item mb-20">
                {{Form::select('price',[''=>'Price']+config('constants.price_option'),request()->get('price',''),['id'=>'car-search-price','class'=>'custom-select'])}}
            </div>

            <div class="holder-item"><button type="submit" class="btn btn-block search_filter">Search</button></div>
        </form>
    </div>
</div>