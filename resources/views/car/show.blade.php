@extends('layouts.frontend')

@push('css')
    <style type="text/css">
        .slider_image{
            height: 402px;
            width: 100%;
            vertical-align: middle;
            display: flex !important;
            align-items: center !important;
            justify-content: center !important;
        }
        .search_filter {
            background-color: #DB0F38 !important;
        }
    </style>
@endpush

@section('content')
<div class="container-outer">
    <div class="container">
        <div class="row gap-25-sm">
            <div class="col-xs-12 col-sm-8 col-md-9">
                <div class="content-wrapper pb-30 pb-0-xs">
                    <div class="detail-titile">
                        <div class="row">
                            <div class="col-xs-12 col-sm-9">
                                <h3 class="car-name">{{$carDetail->brand_name}} {{$carDetail->model_name}}</h3>
                                <div class="detail-location"><i class="fa fa-map-marker"></i> {{$carDetail->city}}, {{$carDetail->country}}</div>
                            </div>
                            <div class="col-xs-12 col-sm-3 text-right text-left-xs">
                                <div class="price text-primary">&euro;{{number_format($carDetail->price)}}</div>
                                <div class="condition">{{$carDetail->condition}}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row gap-5">
                        <div class="col-sm-12 col-md-8">
                            <div class="tab-style-1 detail-tab">
                                <?php  /*if(!in_array($carDetail->id,$currentUserBids)) */ ?>
                                    <div class="tab-style-1-header clearfix">
                                    <!--ul id="detailTab" class="tab-nav clearfix mb-10-xss">
                                    <li class="active"><a href="#detailTab1" data-toggle="tab">Exterior</a></li>
                                    <li><a href="#detailTab2" data-toggle="tab">Interior</a></li>
                                    </ul-->
                                        @php
                                            $image = $carDetail->images->first();
                                            $image = isset($image->image)?$image->image:null;
                                            @endphp
                                        <a href="#" data-image="{{$image}}" data-brand="{{$carDetail->brand_name}}" data-model="{{$carDetail->model_name}}" data-condition="{{$carDetail->condition}}" data-mileage="{{$carDetail->mileage}}" data-price="{{number_format($carDetail->price)}}" data-price2="{{$carDetail->price}}" data-id="{{$carDetail->id}}" class="btn bid_button btn-info pull-right pull-left-xss">Bid Now</a>
                                    </div>
                                <?php /*endif*/?>

                                <div id="myTabContent" class="tab-content" >
                                    <div class="tab-pane fade in active" id="detailTab1">
                                        <div class="tab-content-inner">
                                            <div class="slick-gallery-slideshow">
                                                <div class="slider gallery-slideshow gallery-slideshow-not-tab">
                                                    @foreach($carDetail->images as $value)
                                                        <div class="slider_image">
                                                            <center><div class="image"><img src="{{asset('uploads/car_detail/image/'.$value->image)}}" alt="image" /></div></center>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="slider gallery-nav gallery-nav-not-tab">
                                                    @foreach($carDetail->images as $value)
                                                        <div>
                                                            <div class="image"><img src="{{asset('uploads/car_detail/image/thumbnail/'.$value->image)}}"  alt="image" /></div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="detailTab2">
                                        <div class="tab-content-inner">
                                            <div class="slick-gallery-slideshow">
                                                <div class="slider gallery-slideshow gallery-slideshow-tab-01">
                                                    @foreach($carDetail->images as $value)
                                                        <div class="slider_image">
                                                            <center><div class="image"><img src="{{asset('uploads/car_detail/image/'.$value->image)}}" alt="image" /></div></center>
                                                        </div>
                                                    @endforeach
                                                </div>
                                                <div class="slider gallery-nav gallery-nav-tab-01">
                                                    @foreach($carDetail->images as $value)
                                                        <div>
                                                            <div class="image"><img src="{{asset('uploads/car_detail/image/thumbnail/'.$value->image)}}" alt="image" /></div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="side-module specification-box">
                                <h3>Specification</h3>
                                <div class="inner">
                                    <ul class="specification-list">
                                        <li><span class="absolute">Make:</span> {{$carDetail->brand_name}}</li>
                                        <li><span class="absolute">Model:</span> {{$carDetail->model_name}}</li>
                                        <li><span class="absolute">Type:</span>{{$carDetail->type}}</li>
                                        <li><span class="absolute">Year:</span>{{$carDetail->mfg_year}}</li>
                                        @if($carDetail->vin_number)
                                            <li><span class="absolute">Vin: </span>{{$carDetail->vin_number or ' '}}</li>
                                        @endif
                                        <li><span class="absolute">Fuel Type:</span>{{$carDetail->fuel_type}}</li>
                                        <li><span class="absolute">Abgasnorm: </span>{{$carDetail->abgasnorm}}</li>
                                        <li><span class="absolute">Engine Size: </span>{{$carDetail->engine_size}}</li>
                                        <li><span class="absolute">Engine Power: </span>{{$carDetail->engine_power}}</li>
                                        <li><span class="absolute">CO2 Emission: </span>{{$carDetail->co2_emmision}}</li>
                                        <li><span class="absolute">Transmission:</span> {{$carDetail->transmition}}</li>
                                        <li><span class="absolute">Mileage: </span>{{$carDetail->mileage}}</li>
                                        <li><span class="absolute">All Wheel Drive : </span>{{($carDetail->all_wheel_drive)?'Yes':'No'}}</li>
                                        <li><span class="absolute">Number of gears  : </span>{{$carDetail->number_of_gears}}</li>
                                        <li><span class="absolute">State Coach : </span>{{$carDetail->state_coach}}</li>
                                        <li><span class="absolute">Condition: </span>{{$carDetail->condition}}</li>
                                        @if($carDetail->registration_date)
                                            <li><span class="absolute">Registration Date: </span>{{$carDetail->registration_date}}</li>
                                        @endif
                                        <li><span class="absolute">Condition PDF: </span><a href="{{ url('/').'/uploads/car_detail/pdf/'.$carDetail->condition_pdf }}" download="{{$carDetail->condition_pdf}}" target="_blank"><i class="fa fa-download" title="download Form-11"></i></a></li>
                                        <li class="price"><span class="absolute">Price: </span> &euro;{{number_format($carDetail->price)}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-30"></div>
                    <h3 class="section-title">Full Specification</h3>
                    <h4>{{$carDetail->brand_name}} {{$carDetail->model_name}}</h4>
                    <p>{{$carDetail->description}}</p>
                    <h4>Car's Features</h4>
                    <ul class="list-with-icon">
                        @foreach (explode(',',$carDetail->car_feature) as $value)
                            <li><i class="text-primary fa fa-check"></i> {{$feature[$value]}} </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <aside class="sidebar-wrapper">
                    <div class="row">
                        <div class="col-xss-12 col-xs-6 col-sm-12 col-md-12 mb-30-xss">
                            @include('car.search')
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    <div class="mb-70"></div>
    </div>
</div>
@endsection


