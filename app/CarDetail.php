<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class CarDetail extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id','model_id','fuel_type_id','abgasnorm_id','transmition_id','state_coach_id','country_id','state_id','city_id','created_by','first_registration','type','mfg_year','engine_size','engine_power','co2_emmision','mileage','all_wheel_drive','number_of_gears','condition','condition_pdf','damage','price','description','car_feature','status','bid_status','registration_date','vin_number'
    ];

    protected $table = 'car_detail';


    public function bids(){
        return $this->hasMany('App\BidList')->count();
    }

    public function getRegistrationDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setRegistrationDateAttribute($value)
    {
        if($value != ""){
            return $this->attributes['registration_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }
}
