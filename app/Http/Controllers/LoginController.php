<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Session;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        getSetting();
        // $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        $credentials = $request->only($this->username(), 'password');
        $credentials['status'] = 1;

        return $credentials;
    }

    protected function login(Request $request)
    {
        $validate = $this->validateData($request);

        $userdata = array(
            'email'     => $request->get('email'),
            'password'  => $request->get('password')
        );

        $user = User::where('email',$userdata['email'])->first();

        if(!$user || $user == ''){
            return ['response'=>'0', 'message'=> 'User not exist in system'];
        }

        if(!$user->status || $user->status == '0'){
            return ['response'=>'0', 'message'=> 'User not active. Please contact your admin'];
        }

        if (Auth::attempt($userdata, $request->has('remember')?true:false)) {
            return ['response'=>'1', 'message'=> 'Login Successful'];
        } else {
            return ['response'=>'0', 'message'=> 'Username or password not match'];
        }
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        ];

        return $this->validate($request, $validation);
    }

    public function register(Request $request)
    {
        if(auth()->check()){
            return redirect('/');
        }

        view()->share('pageTitle', 'REGISTER');

        return view('register');
    }

    public function userProfile()
    {
        if(!auth()->check()){
            return redirect('/');
        }

        view()->share('pageTitle', 'EDIT PROFILE');
        view()->share('location', 'Edit Profile');

        view()->share('user',auth()->user());

        return view('myprofile');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function changePassword()
    {
        return view('changePassword');
    }

    /**
     * @param Request $request
     */
    public function updatePassword(Request $request)
    {
        $messages = [
            'current_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        ];

        $this->validate($request,
            [
                'current_password' => 'required',
                'password' => 'required|min:6|max:255',
                'password_confirmation' => 'required|same:password',
            ], $messages);

        $cur_password = $request->input('current_password');

        $user = Auth::user();

        if (Hash::check($cur_password, $user->password)) {
            $user->password = Hash::make($request->input('password'));
            $user->save();

            Session::flash('flash_success','Password changed successfully.');

            return redirect()->back();
        } else {
            // $error = array('current-password' => __('Please enter correct current password'));

            // return redirect()->back()->withErrors($error);
            Session::flash('flash_warning','Please enter correct current password.');

            return redirect()->back();
        }
        Session::flash('flash_error','Something wrong. Please try again latter.');

        return redirect()->back();
    }
}
