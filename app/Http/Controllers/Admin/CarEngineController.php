<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarEngine;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class CarEngineController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.car_engine.index');
    }

    public function datatable(Request $request)
    {
        $carEngine = CarEngine::orderBy('id','DESC')->get();

        return datatables()->of($carEngine)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.car_engine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $carEngine = CarEngine::create($input);
        Session::flash('flash_success', 'Car engine added!');

        return redirect('admin/car-engine');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/car-engine');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$carEngine = CarEngine::where('id',$id)->first();
        if ($carEngine) {
            return view('admin.car_engine.edit', compact('carEngine'));
        } else {
            Session::flash('flash_warning', 'Car engine is not exist!');
            return redirect('admin/car-engine');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);
        
        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $carEngine = CarEngine::where('id',$id)->first();
        $carEngine->update($input);

        Session::flash('flash_success', 'Car engine updated!');

        return redirect('admin/car-engine');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        CarEngine::whereId($id)->delete();
        
        $message = "Car engine Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'name' => ["required",new Unique('car_engine',$id)], //pass table name in constuctor,
        ];
        return $this->validate($request, $validation,[
            'name.required'=>'The Engine Name Field is required'
        ]);
    }
}
