<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use App\CMS;
use App\Rules\Unique;

class CmsController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cms = CMS::where('title', 'LIKE', "%$keyword%")
                ->orWhere('slug', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $cms = CMS::paginate($perPage);
        }

        return view('admin.cms.index', compact('cms'));
    }

    public function datatable(Request $request)
    {
        $cms = CMS::orderBy('id','DESC')->get();

        return datatables()->of($cms)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => ['required',new Unique('cms')],
        ]);

        $requestData = $request->all();

        $requestData['slug'] = str_slug($request->title);

        //image uplode
        if ($request->file('image')) {

            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/cms', $filename);

            $requestData['image'] = $filename;
        }

        $requestData['status'] = isset($requestData['status'])?1:0;

        CMS::create($requestData);

        Session::flash('flash_message', 'CMS added!');

        return redirect('admin/cms');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $cm = CMS::findOrFail($id);

        return view('admin.cms.show', compact('cm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$cm = CMS::findOrFail($id);

        return view('admin.cms.edit', compact('cm'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $requestData = $request->all();

        //image uplode
        if ($request->file('image')) {

            $image = $request->file('image');
            $filename = uniqid(time()) . '.' . $image->getClientOriginalExtension();

            $image->move('uploads/cms', $filename);

            $requestData['image'] = $filename;

        }

        $requestData['status'] = isset($requestData['status'])?1:0;

        $cm = CMS::findOrFail($id);
        $cm->update($requestData);

        Session::flash('flash_message', 'CMS updated!');

        return redirect('admin/cms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    /* public function destroy($id)
    {
        CMS::destroy($id);

        $message = "CMS Deleted !!";

        return response()->json(['message' => $message],200);
    } */
}
