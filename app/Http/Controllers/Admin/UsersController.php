<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Role;
use App\User;
use App\BidList;
use DB;
use Illuminate\Http\Request;
use Session;

class UsersController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.users.index');
    }

    public function datatable(Request $request)
    {
        $users = User::orderBy('id','DESC')->get();

        return datatables()->of($users)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        view()->share('countriesList',$this->countriesList()); // list of all countries

        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rule = [
            'first_name' => 'required',
            'last_name' => 'required',
            'user_name' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'password_confirmation' => 'required_with:password|same:password',
            'mobile' => 'numeric',
            'company_name' => 'required',
            'legal_form' => ['required',function($attribute,$value,$fail) use ($request){
                                $format = ['pdf'];
                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                    $fail('The legal form of company must be a file of type: pdf.');
                                }
                            }],
            'tax_number' => 'required',
            'street' => 'required',
            'number' => 'required',
            'post_code' => 'required',
            'country_id' => 'required',
            'city' => 'required',
        ];

        if($request->has('is_register')){
            $rule['register_accept_checkbox'] = 'required';
        }
        
        $this->validate($request, $rule,[
                'legal_form.required'=>'The legal form of company field is required',
                'country_id.required'=>'The country field is required',
            ]);

        $data = $request->except('password');

        $data['password'] = bcrypt($request->password);

        if($request->has('is_register')){
            $data['status'] = 0;
        }

        $pdf = $data['legal_form'];
        $destinationPath = 'uploads/users/legal_form';
        $data['legal_form'] = time().'-'.$pdf->getClientOriginalName();
        $pdf->move($destinationPath,$data['legal_form']);

        $data['activation_token'] = sha1(time() . uniqid() . $data['email']);
        $user = User::create($data);

        $user->assignRole('CL');

        if($request->has('is_register')){
            event(new Registered($user));

            $this->afterRegister($user);

            Session::flash('flash_success', 'Registration Successfull !');
            return redirect('/');
        }
        Session::flash('flash_success', 'User added!');
        return redirect('admin/users');
    }

    public function afterRegister($user)
    {
        $this->sendEmail($user);
    }

    public function sendEmail(User $user)
    {

        return $user->notify(new  \App\Notifications\ActivationLink($user));
    }

    public function activateAccount($token)
    {

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            Session::flash('flash_error', 'This link is expired.');
            return redirect('/');
        }
        elseif ($user->is_active == 1) {
            Session::flash('flash_success', 'Your account have already activated.Please login!');
            return redirect('/login');
        }

        $user->activation_token = null;
        $user->activation_time = null;
        $user->is_active = 1;
        $user->save();


        if (!$this->guard()->check()) {
           // $this->guard()->login($user);
        }
		// Auth::loginUsingId($user->id);
       return redirect('/login')->with('flash_success', 'Your account has been activated.');
       // return redirect()->intended($this->redirectPath())

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::where('id',$id)->first();

        $user->bids = BidList::select([
                'car_bids.id',
                'car_bids.price',
                'car_bids.status',
                'car_brand.name as brand_name',
                'car_model.name as model_name',
                DB::raw("CONCAT(first_name,' ',last_name) as user_name"),
                'users.mobile'
            ])
            ->join('car_detail',function($join){
                $join->on('car_detail.id','=','car_detail_id');
            })->join('car_brand',function($join){
                $join->on('car_brand.id','=','car_detail.brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','car_detail.model_id');
            })->join('users',function($join){
                $join->on('users.id','=','user_id');
            })
            ->where('car_bids.user_id',$id)
            ->get();

        if ($user) {
            return view('admin.users.show', compact('user'));
        } else {
            Session::flash('flash_message', 'User is not exist!');
            return redirect('admin/users');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$user = User::where('id',$id)->first();
        if ($user) {
            view()->share('countriesList',$this->countriesList()); // list of all countries

            return view('admin.users.edit', compact('user'));
        } else {
            Session::flash('flash_warning', 'User is not exist!');
            return redirect('admin/users');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'user_name' => 'required',
            'mobile' => 'numeric',
            'company_name' => 'required',
            'legal_form' => ['nullable',function($attribute,$value,$fail) use ($request){
                                $format = ['pdf'];
                                if(!in_array($value->getClientOriginalExtension(), $format)){
                                    $fail('The legal form of company must be a file of type: pdf.');
                                }
                            }],
            'tax_number' => 'required',
            'street' => 'required',
            'number' => 'required',
            'post_code' => 'required',
            'country_id' => 'required',
            'city' => 'required',
        ]);


        $requestData = $request->all();

        $user = User::where('id',$id)->first();
        if(isset($requestData['legal_form'])){
            $pdf = $requestData['legal_form'];
            $destinationPath = 'uploads/users/legal_form';
            $requestData['legal_form'] = time().'-'.$pdf->getClientOriginalName();
            $pdf->move($destinationPath,$requestData['legal_form']);
            if($user->legal_form != '' && file_exists($destinationPath.'/'.$user->legal_form)){
                unlink($destinationPath.'/'.$user->legal_form); //delete previously uploaded logo
            }
        }else{
            $requestData['legal_form'] = $requestData['old_legal_form'];
        }

        $user->update($requestData);
        // $user->roles()->detach();

        // $user->assignRole('SU');
        if($request->has('is_register')){
            Session::flash('flash_success', 'Profile Updated Successfully !');
            return redirect()->route('my-account');
        }
        Session::flash('flash_success', 'User updated!');
        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        $user = User::find($id);
        $user->roles()->sync([]);

        $user->delete();

        if($request->has('from_index')){
            $message = "User Deleted !!";

            return response()->json(['message' => $message],200);
        }else{
            Session::flash('flash_success', 'User deleted!');

            return redirect('admin/users');
        }

    }

    public function countriesList()
    {
        return DB::table('countries')->pluck('name','id')->toArray();
    }
}
