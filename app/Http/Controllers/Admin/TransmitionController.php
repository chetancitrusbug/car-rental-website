<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Transmition;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class TransmitionController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.transmition.index');
    }

    public function datatable(Request $request)
    {
        $transmition = Transmition::orderBy('id','DESC')->get();

        return datatables()->of($transmition)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.transmition.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $transmition = Transmition::create($input);
        Session::flash('flash_success', 'Transmition added!');

        return redirect('admin/transmition');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/transmition');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$transmition = Transmition::where('id',$id)->first();
        if ($transmition) {
            return view('admin.transmition.edit', compact('transmition'));
        } else {
            Session::flash('flash_warning', 'Transmition is not exist!');
            return redirect('admin/transmition');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);
        
        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $transmition = Transmition::where('id',$id)->first();
        $transmition->update($input);

        Session::flash('flash_success', 'Transmition updated!');

        return redirect('admin/transmition');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Transmition::whereId($id)->delete();
        
        $message = "Transmition Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'title' => ["required",new Unique('transmition',$id)], //pass table name in constuctor
        ];
                
        return $this->validate($request, $validation);
    }
}
