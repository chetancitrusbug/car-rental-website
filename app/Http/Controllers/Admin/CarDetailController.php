<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarDetail;
use App\CarImages;
use App\CarBrand;
use App\CarModel;
use App\FuelType;
use App\Abgasnorm;
use App\Transmition;
use App\StateCoach;
use App\Feature;
use App\User;
use App\BidList;
use Illuminate\Http\Request;
use Session;
use DB;
use Image;
use File;
use App\Rules\Unique;

class CarDetailController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        // echo '<pre>'; print_r($this->getCarDetail($request)); exit;
        return view('admin.car_detail.index');
    }

    public function datatable(Request $request)
    {
        $carDetail = $this->getCarDetail($request);

        return datatables()->of($carDetail)->make(true);
    }

    public function getCarDetail($request)
    {
        $carDetail = CarDetail::select([
                'car_detail.id',
                'car_brand.name as brand_name',
                'car_brand.status as brand_status',
                'car_model.name as model_name',
                'car_model.status as model_status',
                'fuel_type.status as fuel_type_status',
                'abgasnorm.status as abgasnorm_status',
                'state_coach.status as state_coach_status',
                'transmition.status as transmition_status',
                'car_detail.type',
                'car_detail.description',
                'car_detail.mfg_year',
                'car_detail.mileage',
                'car_detail.price',
                'car_detail.status',
                'car_detail.bid_status',
                'car_detail.total_bids as bid_count',
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })
            ->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })
            ->leftJoin('fuel_type',function($join){
                $join->on('fuel_type.id','=','car_detail.fuel_type_id');
            })
            ->leftJoin('abgasnorm',function($join){
                $join->on('abgasnorm.id','=','car_detail.abgasnorm_id');
            })
            ->leftJoin('state_coach',function($join){
                $join->on('state_coach.id','=','car_detail.state_coach_id');
            })
            ->leftJoin('transmition',function($join){
                $join->on('transmition.id','=','car_detail.transmition_id');
            });

        if($request->get('brand_id',0)){
            $carDetail->where('car_detail.brand_id',$request->get('brand_id'));
        }

        if($request->get('model_id',0)){
            $carDetail->where('car_detail.model_id',$request->get('model_id'));
        }
        return $carDetail->orderBy('car_detail.id','DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        view()->share('brandList',$this->brandList()); // list of all brands
        view()->share('fuelTypeList',$this->fuelTypeList()); // list of all fuel type
        view()->share('abgasnormList',$this->abgasnormList()); // list of all abgasnorm
        view()->share('transmitionList',$this->transmitionList()); // list of all transmition
        view()->share('stateCoachList',$this->stateCoachList()); // list of all state coach
        view()->share('carFeatureList',$this->carFeatureList()); // list of all state coach
        view()->share('mfgYear',$this->getMfgYear()); // list of all state coach
        view()->share('countriesList',$this->countriesList()); // list of all countries
        view()->share('userList',$this->userList()); // list of all users

        $modelList = $statesList = $citiesList = [];
        if(old('brand_id',null)){
            $modelList = $this->modelList($request,['brand_id'=>old('brand_id',null)]);
        }

        if(old('country_id',null)){
            $statesList = $this->statesList($request,['country_id'=>old('country_id',null)]);
        }

        if(old('state_id',null)){
            $citiesList = $this->citiesList($request,['state_id'=>old('state_id',null)]);
        }
        view()->share('modelList',$modelList); // list of all models
        view()->share('statesList',$statesList); // list of all states
        view()->share('citiesList',$citiesList); // list of all cities
        return view('admin.car_detail.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['car_feature'] = implode(',', $input['car_feature']);

        $pdf = $input['condition_pdf'];
        $destinationPath = 'uploads/car_detail/pdf';
        $input['condition_pdf'] = time().'-'.$pdf->getClientOriginalName();
        $pdf->move($destinationPath,$input['condition_pdf']);

        $input['status'] = isset($input['status'])?$input['status']:0;
        $input['bid_status'] = isset($input['bid_status'])?$input['bid_status']:0;
        $input['all_wheel_drive'] = isset($input['all_wheel_drive'])?$input['all_wheel_drive']:0;
        $input['first_registration'] = isset($input['first_registration'])?$input['first_registration']:0;
        $input['registration_date'] = \Carbon\Carbon::parse($input['registration_date'])->format('Y-m-d');

        $carDetail = CarDetail::create(array_except($input,['image']));
        //upload image
        $this->uploadImage($input['image'],$carDetail->id);

        Session::flash('flash_success', 'Car added!');

        return redirect('admin/car-detail');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/car-detail');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id,Request $request)
    {

        view()->share('brandList',$this->brandList()); // list of all brands
        view()->share('fuelTypeList',$this->fuelTypeList()); // list of all fuel type
        view()->share('abgasnormList',$this->abgasnormList()); // list of all abgasnorm
        view()->share('transmitionList',$this->transmitionList()); // list of all transmition
        view()->share('stateCoachList',$this->stateCoachList()); // list of all state coach
        view()->share('carFeatureList',$this->carFeatureList()); // list of all state coach
        view()->share('mfgYear',$this->getMfgYear()); // list of all state coach
        view()->share('countriesList',$this->countriesList()); // list of all countries
        view()->share('userList',$this->userList()); // list of all users

        $carDetail = CarDetail::select([
                'car_detail.*',
                'car_brand.name as brand_name',
                'car_brand.status as brand_status',
                'car_model.name as model_name',
                'car_model.status as model_status',
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })
            ->where('car_detail.id',$id)
            ->first();

        if($carDetail->brand_status == '0' || $carDetail->model_status == '0'){
            Session::flash('flash_warning', 'Not able to edit!');
            return redirect('/admin/car-detail');
        }
        $carDetail->images = CarImages::select(['id','image'])->where('car_detail_id',$id)->get()->toArray();

        $modelList = $statesList = $citiesList = [];
        if(old('brand_id',$carDetail->brand_id)){
            $modelList = $this->modelList($request,['brand_id'=>old('brand_id',$carDetail->brand_id)]);
        }

        if(old('country_id',$carDetail->country_id)){
            $statesList = $this->statesList($request,['country_id'=>old('country_id',$carDetail->country_id)]);
        }

        if(old('state_id',$carDetail->state_id)){
            $citiesList = $this->citiesList($request,['state_id'=>old('state_id',$carDetail->state_id)]);
        }
        view()->share('modelList',$modelList); // list of all models
        view()->share('statesList',$statesList); // list of all states
        view()->share('citiesList',$citiesList); // list of all cities

        if ($carDetail) {
            return view('admin.car_detail.edit', compact('carDetail'));
        } else {
            Session::flash('flash_warning', 'Car is not exist!');
            return redirect('admin/car-detail');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);

        $input['car_feature'] = implode(',', $input['car_feature']);

        if(isset($input['condition_pdf'])){
            $pdf = $input['condition_pdf'];
            $destinationPath = 'uploads/car_detail/pdf';
            $input['condition_pdf'] = time().'-'.$pdf->getClientOriginalName();
            $pdf->move($destinationPath,$input['condition_pdf']);
            if(file_exists($destinationPath.'/'.$input['condition_pdf'])){
                unlink($destinationPath.'/'.$input['condition_pdf']); //delete previously uploaded logo
            }
        }else{
            $input['condition_pdf'] = $input['old_condition_pdf'];
        }

        $input['status'] = isset($input['status'])?$input['status']:0;
        $input['bid_status'] = isset($input['bid_status'])?$input['bid_status']:0;
        $input['all_wheel_drive'] = isset($input['all_wheel_drive'])?$input['all_wheel_drive']:0;
        $input['first_registration'] = isset($input['first_registration'])?$input['first_registration']:0;
        $input['registration_date'] = \Carbon\Carbon::parse($input['registration_date'])->format('Y-m-d');
        
        $carDetail = CarDetail::where('id',$id)->update(array_except($input,['old_condition_pdf','image']));

        if(!empty($input['image'])){
            CarImages::where('car_detail_id',$id)->update(['deleted_at'=>now()]);
            //upload image
            $this->uploadImage($input['image'],$id);
        }

        Session::flash('flash_success', 'Car updated!');

        return redirect('admin/car-detail');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        CarDetail::whereId($id)->delete();
        CarImages::where('car_detail_id',$id)->delete();

        $message = "Car Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'brand_id' => 'required',
            'model_id' => 'required',
            'fuel_type_id' => 'required',
            'abgasnorm_id' => 'required',
            'transmition_id' => 'required',
            'state_coach_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'vin_number' => 'required',
            'type' => 'required',
            'mfg_year' => 'required',
            'engine_size' => 'required',
            'engine_power' => 'required',
            'registration_date' => 'required|date|date_format:d-m-Y|before:today',
            'co2_emmision' => 'required',
            'mileage' => 'required|numeric',
            'number_of_gears' => 'required',
            'condition' => 'required',
            'condition_pdf' => 'mimes:pdf',
            'damage' => 'required',
            'price' => 'required',
            'description' => 'required',
            'car_feature' => 'required',
        ];

        if(!empty($request['image'])){
            foreach ($request['image'] as $key => $value) {
                $validation['image.'.$key] = 'mimes:jpg,jpeg,png';
            }
        }

        if($request->get('old_condition_pdf') == ''){
            $validation['condition_pdf'] .= '|required';
        }
        return $this->validate($request, $validation,[
            'brand_id.required'=>'The Brand field is required.',
            'model_id.required'=>'The Model field is required.',
            'fuel_type_id.required'=>'The Fuel Type field is required.',
            'abgasnorm_id.required'=>'The Abgasnorm field is required.',
            'transmition_id.required'=>'The Transmition field is required.',
            'state_coach_id.required'=>'The State Coach field is required.',
            'country_id.required'=>'The Country field is required.',
            'state_id.required'=>'The State field is required.',
            'city_id.required'=>'The City field is required.',
        ]);
    }

    public function brandList()
    {
        return CarBrand::where("status","1")->pluck('name','id')->toArray();
    }

    public function modelList(Request $request,$param=[])
    {
        $object = CarModel::where("status","1");
        if($request->has('brand_id') || $param['brand_id']){
            $object->where('brand_id',($request->has('brand_id'))?$request->get('brand_id'):$param['brand_id']);
        }
        return $object->pluck('name','id')->toArray();
    }

    public function countriesList()
    {
        return DB::table('countries')->pluck('name','id')->toArray();
    }

    public function statesList(Request $request,$param=[])
    {
        $object = DB::table('states');
        if($request->has('country_id') || $param['country_id']){
            $object->where('country_id',($request->has('country_id'))?$request->get('country_id'):$param['country_id']);
        }
        return $object->pluck('name','id')->toArray();
    }

    public function citiesList(Request $request,$param=[])
    {
        $object = DB::table('cities');
        if($request->has('state_id') || $param['state_id']){
            $object->where('state_id',($request->has('state_id'))?$request->get('state_id'):$param['state_id']);
        }
        return $object->pluck('name','id')->toArray();
    }

    public function fuelTypeList()
    {
        return FuelType::where("status","1")->pluck('title','id')->toArray();
    }

    public function abgasnormList()
    {
        return Abgasnorm::where("status","1")->pluck('title','id')->toArray();
    }

    public function transmitionList()
    {
        return Transmition::where("status","1")->pluck('title','id')->toArray();
    }

    public function stateCoachList()
    {
        return StateCoach::where("status","1")->pluck('title','id')->toArray();
    }

    public function carFeatureList()
    {
        return Feature::where("status","1")->pluck('title','id')->toArray();
    }

    public function userList()
    {
        return User::select(DB::raw("CONCAT(first_name,' ',last_name) AS name"),'id')->where("status","1")->pluck('name','id')->toArray();
    }

    public function getMfgYear()
    {
        $year = [];
        $currentYear = \Carbon\Carbon::now()->format('Y');
        for ($i = config('constants.car_detail.year.start'); $i <= $currentYear; $i++) {
            $year[$i] = $i;
        }
        return $year;
    }

    public function uploadImage($image = [], $id=0)
    {
        foreach ($image as $key => $value) {
            $image[$key] = Image::make($value->getRealPath());
            $thumbImage[$key] = Image::make($value->getRealPath());

            $thumbPath = public_path() . '/uploads/car_detail/image/thumbnail';
            File::exists($thumbPath) or File::makeDirectory($thumbPath);

            $imagePath = public_path() . '/uploads/car_detail/image';
            File::exists($imagePath) or File::makeDirectory($imagePath);

            $imageName = uniqid().'_'.$value->getClientOriginalName();

            // save thumbnail image
            $image[$key]->fit(150, 150, function ($constraint) {
                $constraint->aspectRatio();
            })->save($thumbPath.'/'.$imageName);

            if($thumbImage[$key]->height() > 410 || $thumbImage[$key]->width() > 560){
                $thumbImage[$key]->fit(560, 410, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($imagePath.'/'.$imageName);
            }else{
                $thumbImage[$key]->save($imagePath.'/'.$imageName);
            }

            $insert['car_detail_id'] = $id;
            $insert['image'] = $imageName;
            $insert['status'] = '1';
            $insert['is_feature'] = '1';

            CarImages::create($insert);
        }
    }

    public function bidList($id)
    {
        $carDetail = CarDetail::select([
                'car_detail.id',
                'car_brand.name as brand_name',
                'car_model.name as model_name',
                'car_detail.type',
                'car_detail.price',
                'car_detail.status',
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })
            ->find($id);

        return view('admin.car_detail.bid_list',['id'=>$id,'carDetail'=>$carDetail]);
    }

    public function bidDatatable(Request $request,$id)
    {
        $carBids = BidList::select([
                'car_bids.id',
                'car_bids.price',
                'car_bids.status',
                'car_bids.car_detail_id',
                'car_brand.id as brand_id',
                'car_brand.name as brand_name',
                'car_model.id as model_id',
                'car_model.name as model_name',
                'car_detail.approve_bid_user_id',
                DB::raw("CONCAT(first_name,' ',last_name) as user_name"),
                'users.mobile'
            ])
            ->join('car_detail',function($join){
                $join->on('car_detail.id','=','car_detail_id');
            })->join('car_brand',function($join){
                $join->on('car_brand.id','=','car_detail.brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','car_detail.model_id');
            })->join('users',function($join){
                $join->on('users.id','=','user_id');
            })
            ->where('car_detail.id',$id)
            ->get();

        return datatables()->of($carBids)->make(true);
    }

    public function changeStatus($table,$status,$id)
    {
        $status = ($status == 0)?1:0;

        DB::table($table)->where('id',$id)->update(['status'=>$status]);
        if($table == 'car_brand'){
            DB::table('car_model')->where('brand_id',$id)->update(['status'=>$status]);
            DB::table('car_detail')->where('brand_id',$id)->update(['status'=>$status]);
        }elseif($table == 'car_model'){
            DB::table('car_detail')->where('model_id',$id)->update(['status'=>$status]);
        }elseif($table == 'car_detail'){
            $detail = DB::table('car_detail')->where('id',$id)->first();

            if($detail->approve_bid_user_id == 0 && $status == 0){
                $bid = BidList::select(['user_id','price'])
                        ->where('car_detail_id',$id)
                        ->get();

                $carBrand = CarBrand::where('id',$detail->brand_id)->first();
                $carModel = CarModel::where('id',$detail->model_id)->first();

                $brand = $carBrand->name;
                $model = $carModel->name;

                foreach ($bid as $key => $value) {
                    $user = User::select(['email','first_name','last_name'])->where('id',$value->user_id)->first();
                    $user->brand = $brand;
                    $user->model = $model;
                    $user->price = $value->price;

                    $user->notify(new  \App\Notifications\BidReject($user));
                }
            }
        }

        return response()->json(['message' => 'Status Updated'],200);
    }

    public function approveBid(Request $request)
    {
        $bid = BidList::where('id',$request->get('bid_id'))->first();

        BidList::where('id',$request->get('bid_id'))->update(['status'=>'1']);

        //update approve bid user_id in car_detail table
        CarDetail::where('id',$request->get('car_detail_id'))->update(['approve_bid_user_id'=>$bid->user_id]);

        $user = User::select(['email','first_name','last_name'])->where('id',$bid->user_id)->first();

        $carBrand = CarBrand::where('id',$request->get('brand_id'))->first();
        $carModel = CarModel::where('id',$request->get('model_id'))->first();

        $brand = $carBrand->name;
        $model = $carModel->name;

        //send mail to bid confirm user
        $user->price = $bid->price;
        $user->brand = $brand;
        $user->model = $model;

        $user->notify(new  \App\Notifications\BidApprove($user));

        //send reject mail to user

        BidList::select(['user_id','price'])
                ->whereNotIn('id',[$request->get('bid_id')])
                ->where('car_detail_id',$request->get('car_detail_id'))
                ->update(['status'=>'0']);

        $bid = BidList::select(['user_id','price'])
                ->whereNotIn('id',[$request->get('bid_id')])
                ->where('car_detail_id',$request->get('car_detail_id'))
                ->get();

        foreach ($bid as $key => $value) {
            $user = User::select(['email','first_name','last_name'])->where('id',$value->user_id)->first();
            $user->brand = $brand;
            $user->model = $model;
            $user->price = $value->price;

            $user->notify(new  \App\Notifications\BidReject($user));
        }
        CarDetail::find($request->get('car_detail_id'))->update(['bid_status'=>"0"]);
        return response()->json(['message' => 'Bid approved and mail send successfully'],200);
    }
}
