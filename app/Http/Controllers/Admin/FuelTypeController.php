<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\FuelType;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class FuelTypeController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.fuel_type.index');
    }

    public function datatable(Request $request)
    {
        $fuelType = FuelType::orderBy('id','DESC')->get();

        return datatables()->of($fuelType)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.fuel_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $fuelType = FuelType::create($input);
        Session::flash('flash_success', 'Fuel Type added!');

        return redirect('admin/fuel-type');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/fuel-type');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$fuelType = FuelType::where('id',$id)->first();
        if ($fuelType) {
            return view('admin.fuel_type.edit', compact('fuelType'));
        } else {
            Session::flash('flash_warning', 'Fuel Type is not exist!');
            return redirect('admin/fuel-type');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);
        
        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $fuelType = FuelType::where('id',$id)->first();
        $fuelType->update($input);

        Session::flash('flash_success', 'Fuel Type updated!');

        return redirect('admin/fuel-type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        FuelType::whereId($id)->delete();
        
        $message = "Fuel Type Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'title' => ["required",new Unique('fuel_type',$id)], //pass table name in constuctor
        ];
                
        return $this->validate($request, $validation);
    }
}
