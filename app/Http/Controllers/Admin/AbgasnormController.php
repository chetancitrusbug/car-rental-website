<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Abgasnorm;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class AbgasnormController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.abgasnorm.index');
    }

    public function datatable(Request $request)
    {
        $abgasnorm = Abgasnorm::orderBy('id','DESC')->get();

        return datatables()->of($abgasnorm)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.abgasnorm.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $abgasnorm = Abgasnorm::create($input);
        Session::flash('flash_success', 'Abgasnorm added!');

        return redirect('admin/abgasnorm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/abgasnorm');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$abgasnorm = Abgasnorm::where('id',$id)->first();
        if ($abgasnorm) {
            return view('admin.abgasnorm.edit', compact('abgasnorm'));
        } else {
            Session::flash('flash_warning', 'Abgasnorm is not exist!');
            return redirect('admin/abgasnorm');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);
        
        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $abgasnorm = Abgasnorm::where('id',$id)->first();
        $abgasnorm->update($input);

        Session::flash('flash_success', 'Abgasnorm updated!');

        return redirect('admin/abgasnorm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Abgasnorm::whereId($id)->delete();
        
        $message = "Abgasnorm Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'title' => ["required",new Unique('abgasnorm',$id)], //pass table name in constuctor
        ];
                
        return $this->validate($request, $validation);
    }
}
