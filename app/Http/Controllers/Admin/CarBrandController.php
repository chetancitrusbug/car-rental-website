<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarBrand;
use App\CarModel;
use App\CarDetail;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class CarBrandController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.car_brand.index');
    }

    public function datatable(Request $request)
    {
        $carBrand = CarBrand::orderBy('id','DESC')->get();

        return datatables()->of($carBrand)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.car_brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $logo = $input['logo'];
        $destinationPath = 'uploads/brand';
        $input['logo'] = time().'-'.$logo->getClientOriginalName();
        $logo->move($destinationPath,$input['logo']);

        $input['status'] = isset($input['status'])?$input['status']:0;

        $carBrand = CarBrand::create($input);
        Session::flash('flash_success', 'Car brand added!');

        return redirect('admin/car-brand');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/car-brand');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$carBrand = CarBrand::where('id',$id)->first();
        if ($carBrand) {
            return view('admin.car_brand.edit', compact('carBrand'));
        } else {
            Session::flash('flash_warning', 'Car Brand is not exist!');
            return redirect('admin/car-brand');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);

        if(isset($input['logo'])){
            $logo = $input['logo'];
            $destinationPath = 'uploads/brand';
            $input['logo'] = time().'-'.$logo->getClientOriginalName();
            $logo->move($destinationPath,$input['logo']);
            if(file_exists($destinationPath.'/'.$input['old_logo'])){
                unlink($destinationPath.'/'.$input['old_logo']); //delete previously uploaded logo
            }
        }else{
            $input['logo'] = $input['old_logo'];
        }

        $input['status'] = isset($input['status'])?$input['status']:0;

        $carBrand = CarBrand::where('id',$id)->first();

        if($input['status'] != $carBrand->status){
            CarModel::where('brand_id',$id)->update(['status'=>$input['status']]);
            CarDetail::where('brand_id',$id)->update(['status'=>$input['status']]);
        }

        $carBrand->update(array_except($input,'old_logo'));

        Session::flash('flash_success', 'Car brand updated!');

        return redirect('admin/car-brand');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        CarBrand::whereId($id)->delete();
        CarModel::where('brand_id',$id)->delete();
        CarDetail::where('brand_id',$id)->delete();

        $message = "Car brand Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'name' => ["required","regex:/^[a-z0-9 .\-]+$/i",new Unique('car_brand',$id)], //pass table name in constuctor
            'logo' => 'mimes:jpeg,jpg,png'
        ];
        if($request->get('old_logo') == ''){
            $validation['logo'] .= '|required';
        }
        return $this->validate($request, $validation,[
            'name.required'=>'The Brand Name Field is required',
            'name.regex'=>'The Brand Name format is invalid',
        ]);
    }
}
