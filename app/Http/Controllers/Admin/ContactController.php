<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Contact;
use DB;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class ContactController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.contact.index');
    }

    public function datatable(Request $request)
    {
        $contact = Contact::select([
            'contact.*',
            DB::raw("DATE_FORMAT(created_at,'%d-%m-%Y %H:%i:%s') as created_atdate")
        ])->orderBy('id','DESC')->get();

        return datatables()->of($contact)->make(true) ;
    }

    public function show($id)
    {
        $contact = Contact::findOrFail($id);

        return view('admin.contact.show', compact('contact'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy(Request $request,$id)
    {
        Contact::whereId($id)->delete();

        $message = "Contact detail Deleted !!";
        if($request->has('view')){
            Session::flash('flash_message', $message);
            return redirect('/admin/contact');
        }
        return response()->json(['message' => $message],200);
    }
}
