<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Feature;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class FeatureController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.feature.index');
    }

    public function datatable(Request $request)
    {
        $feature = Feature::orderBy('id','DESC')->get();

        return datatables()->of($feature)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.feature.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $feature = Feature::create($input);
        Session::flash('flash_success', 'Feature added!');

        return redirect('admin/feature');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/feature');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
		$feature = Feature::where('id',$id)->first();
        if ($feature) {
            return view('admin.feature.edit', compact('feature'));
        } else {
            Session::flash('flash_warning', 'Feature is not exist!');
            return redirect('admin/feature');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);
        
        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $feature = Feature::where('id',$id)->first();
        $feature->update($input);

        Session::flash('flash_success', 'Feature updated!');

        return redirect('admin/feature');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        Feature::whereId($id)->delete();
        
        $message = "Feature Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'title' => ["required",new Unique('feature',$id)], //pass table name in constuctor
        ];
                
        return $this->validate($request, $validation);
    }
}
