<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\StateCoach;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class StateCoachController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.state_coach.index');
    }

    public function datatable(Request $request)
    {
        $stateCoach = StateCoach::orderBy('id','DESC')->get();

        return datatables()->of($stateCoach)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        return view('admin.state_coach.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $stateCoach = StateCoach::create($input);
        Session::flash('flash_success', 'State coach added!');

        return redirect('admin/state-coach');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/state-coach');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        $stateCoach = StateCoach::where('id',$id)->first();
        
        if ($stateCoach) {
            return view('admin.state_coach.edit', compact('stateCoach'));
        } else {
            Session::flash('flash_warning', 'State coach is not exist!');
            return redirect('admin/state-coach');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);

        $input['status'] = isset($input['status'])?$input['status']:0;
        
        $stateCoach = StateCoach::where('id',$id)->first();
        $stateCoach->update($input);

        Session::flash('flash_success', 'State coach updated!');

        return redirect('admin/state-coach');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        StateCoach::whereId($id)->delete();
        
        $message = "State coach Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'title' => ["required",new Unique('state_coach',$id)], //pass table name in constuctor
        ];

        return $this->validate($request, $validation);
    }
}
