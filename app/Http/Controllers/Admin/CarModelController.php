<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\CarBrand;
use App\CarModel;
use App\CarDetail;
use Illuminate\Http\Request;
use Session;
use App\Rules\Unique;

class CarModelController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {
        return view('admin.car_model.index');
    }

    public function datatable(Request $request)
    {
        $carModel = CarModel::select([
                'car_model.*',
                'car_brand.name as brand_name',
                'car_brand.status as brand_status',
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            });

        if($request->get('brand_id',0)){
            $carModel->where('car_model.brand_id',$request->get('brand_id'));
        }

        $carModel = $carModel->orderBy('car_model.id','DESC')->get();

        return datatables()->of($carModel)->make(true) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        view()->share('brandList',$this->brandList()); // list on all brands

        return view('admin.car_model.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $input = $request->except('_token');
        $validate = $this->validateData($request);

        $input['status'] = isset($input['status'])?$input['status']:0;

        $carModel = CarModel::create($input);
        Session::flash('flash_success', 'Car model added!');

        return redirect('admin/car-model');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        return redirect('admin/car-model');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        view()->share('brandList',$this->brandList()); // list on all brands

        $carModel = CarModel::where('id',$id)->first();

        if ($carModel) {
            return view('admin.car_model.edit', compact('carModel'));
        } else {
            Session::flash('flash_warning', 'Car Model is not exist!');
            return redirect('admin/car-model');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $input = $request->except(['_token','_method']);
        $validate = $this->validateData($request,$id);

        $input['status'] = isset($input['status'])?$input['status']:0;

        $carModel = CarModel::where('id',$id)->first();

        if($input['status'] != $carModel->status){
            CarDetail::where('model_id',$id)->update(['status'=>$input['status']]);
        }

        $carModel->update($input);

        Session::flash('flash_success', 'Car model updated!');

        return redirect('admin/car-model');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        CarModel::whereId($id)->delete();
        CarDetail::where('model_id',$id)->delete();

        $message = "Car model Deleted !!";

        return response()->json(['message' => $message],200);
    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'name' => ["required",new Unique('car_model',$id)], //pass table name in constuctor
            'brand_id' => 'required'
        ];

        return $this->validate($request, $validation,[
            'name.required'=>'The Model Name Field is required',
            'brand_id.required'=>'The Car Brand Field is required',
        ]);
    }

    public function brandList()
    {
        return CarBrand::where("status","1")->pluck('name','id')->toArray();
    }
}
