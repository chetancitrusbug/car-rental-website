<?php

namespace App\Http\Controllers;

use DB;
use App\CarDetail;
use App\CarImages;
use App\CarModel;
use App\CarBrand;
use App\Feature;
use App\BidList;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function __construct()
    {
        getSetting();
        $this->middleware('client_auth');
    }

    //
    public function index(Request $request,$brandId = null){
        view()->share('pageTitle', 'RESULT');
        view()->share('location', 'Result');

        $currentUserBids = BidList::where('user_id',auth()->id())->pluck('car_detail_id','car_detail_id')->toArray();
        view()->share('currentUserBids', $currentUserBids);
        view()->share('brandId', $brandId);

        $this->getBrandAndYearList();

        $carDetail = $this->getCarDetail($request,0,$brandId);

        return view('car.index',['carDetail'=>$carDetail,'filter'=>$request->get('filter',null)]);
    }

    public function getCarDetail($request,$recent = 0,$brandId = 0, $myaccount = true){
        $setting = \App\Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);
        
        $carDetail = CarDetail::select([
                'car_detail.id',
                'car_brand.name as brand_name',
                'car_model.name as model_name',
                'car_detail.type',
                'car_detail.description',
                'car_detail.condition',
                'car_detail.mfg_year',
                'car_detail.engine_size',
                'fuel_type.title as fuel_type',
                'car_detail.mileage',
                'cities.name as location',
                'car_detail.price',
                'car_detail.status',
                'car_detail.bid_status',
                'car_detail.approve_bid_user_id',
                'car_images.image'
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })->leftJoin('car_images',function($join){
                $join->on('car_images.car_detail_id','=','car_detail.id');
            })->leftJoin('fuel_type',function($join){
                $join->on('fuel_type.id','=','car_detail.fuel_type_id');
            })->leftJoin('cities',function($join){
                $join->on('cities.id','=','car_detail.city_id');
            })
            ->whereNull('car_images.deleted_at')
            ->where('car_detail.status',"1")
            ->where('car_brand.status',"1")
            ->where('car_model.status',"1")
            ->groupBy('car_images.car_detail_id');

        if($myaccount){
            $carDetail->where('car_detail.bid_status',"1");
        }

        if($request->get('brand_id',$brandId)){
            $carDetail->where('car_detail.brand_id',$request->get('brand_id',$brandId));
        }

        if($request->get('model_id',0)){
            $carDetail->where('car_detail.model_id',$request->get('model_id'));
        }

        if($request->get('year',null)){
            $carDetail->where('car_detail.mfg_year',$request->get('year'));
        }
        if($request->has('filter')){
            if($request->get('filter') == '1'){
                $carDetail->orderBy('car_brand.name');
            }elseif($request->get('filter') == '2'){
                $carDetail->orderBy(DB::raw('CAST(`car_detail`.`price` AS DECIMAL(18,4))'));
            }elseif($request->get('filter') == '3'){
                $carDetail->orderBy('fuel_type.title');
            }elseif($request->get('filter') == '4'){
                $carDetail->orderBy('car_detail.mfg_year');
            }elseif($request->get('filter') == '5'){
                $carDetail->orderBy('car_model.name');
            }
        }else{
            $carDetail->orderBy('car_detail.id','DESC');
        }

        if($request->get('price',null)){
            $filter = config('constants.price_filter'); //mysql where condition set in condtants config
            $price = $request->get('price',null);

            $carDetail->whereRaw("car_detail.price ".$filter[$price]); //price filter from car list
        }
        if($recent > 0){
            $carDetail->orderBy('id','DESC')->limit(4)->get();
        }
        return $carDetail->paginate(15);
    }

    public function show($id,Request $request)
    {
        $setting = \App\Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);
        
        view()->share('pageTitle', 'CAR DETAILS');
        view()->share('location', 'Car Details');

        view()->share('brandId', '');

        $currentUserBids = BidList::where('user_id',auth()->id())->pluck('car_detail_id','car_detail_id')->toArray();
        view()->share('currentUserBids', $currentUserBids);

        $this->getBrandAndYearList();

        $carDetail = CarDetail::select([
                'car_detail.*',
                'car_brand.name as brand_name',
                'car_model.name as model_name',
                'fuel_type.title as fuel_type',
                'abgasnorm.title as abgasnorm',
                'state_coach.title as state_coach',
                'transmition.title as transmition',
                'cities.name as city',
                'countries.name as country',
                DB::raw('count(car_bids.car_detail_id) as bid_count')
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })->leftJoin('car_bids',function($join){
                $join->on('car_bids.car_detail_id','=','car_detail.id');
            })->leftJoin('fuel_type',function($join){
                $join->on('fuel_type.id','=','car_detail.fuel_type_id');
            })->leftJoin('cities',function($join){
                $join->on('cities.id','=','car_detail.city_id');
            })->leftJoin('countries',function($join){
                $join->on('countries.id','=','car_detail.country_id');
            })->leftJoin('abgasnorm',function($join){
                $join->on('abgasnorm.id','=','car_detail.abgasnorm_id');
            })->leftJoin('state_coach',function($join){
                $join->on('state_coach.id','=','car_detail.state_coach_id');
            })->leftJoin('transmition',function($join){
                $join->on('transmition.id','=','car_detail.transmition_id');
            })
            ->groupBy('car_bids.car_detail_id')
            ->where('car_detail.id',$id)
            ->first();

        if($carDetail->status == '0'){
            return redirect('/car-list');
        }

        $carDetail->images = CarImages::where('car_detail_id',$id)->get();
        $feature = Feature::pluck('title','id')->toArray();

        view()->share('feature', $feature);

        return view('car.show',['carDetail'=>$carDetail]);
    }

    public function create(){

        view()->share('pageTitle', 'ADD YOUR CAR');
        view()->share('location', 'Add Your Car');

        return view('car.create');
    }

    public function modelList(Request $request,$param=[])
    {
        $object = CarModel::where("status","1");
        if($request->has('brand_id') || (isset($param['brand_id']) && !empty($param['brand_id']))){
            $object->where('brand_id',($request->has('brand_id'))?$request->get('brand_id'):$param['brand_id']);
        }
        return $object->pluck('name','id')->toArray();
    }

    public function bidStore(Request $request,$id)
    {
        if(auth()->check()){
            $this->validate($request, [
                'user_id'=>'required',
                'car_detail_id'=>'required',
                // 'price'=>'required|numeric',
            ],[
                'user_id.required'=>'User detail not found',
                'car_detail_id.required'=>'Car detail not found',
            ]);

            /*if($request->get('price') < $request->get('car_price')){
                return ['response'=>'1','message'=>'Bid price must be greater than car price'];
            }*/

            // $check = BidList::where('user_id',$request->get('user_id'))->where('car_detail_id',$request->get('car_detail_id'))->first();

            // if($check){
            //     return ['response'=>'1','message'=>'bid already exist'];
            // }

            $car = CarDetail::find($request->get('car_detail_id'));
            $car->total_bids = $car->total_bids+1;
            $price = $car->price = $car->price+100;
            $carBrand = CarBrand::where('id',$car->brand_id)->first();
            $carModel = CarModel::where('id',$car->model_id)->first();
            $car->save();
            
            $bidInput = $request->except(['_token','car_price']);
            $bidInput['price'] = $price;
            $bid = BidList::create($bidInput);

            $user = auth()->user();
            $user->price = $price;
            $user->brand = $carBrand->name;
            $user->model = $carModel->name;

            $user->notify(new  \App\Notifications\BidEmail($user));

            return $bid->id;
        }
        return ['response'=>'0','message'=>'Please Login First'];
    }

    public function getBrandAndYearList(){
        $brandList = brandList();
        view()->share('brandList', $brandList);

        $mfgYear = getMfgYear();
        view()->share('mfgYear', $mfgYear);
    }

    public function bidShow(Request $request,$id){
        $carDetail = CarDetail::select([
            'car_detail.*',
            'car_brand.name as brand_name',
            'car_model.name as model_name',
            
        ])
        ->join('car_brand',function($join){
            $join->on('car_brand.id','=','brand_id');
        })->join('car_model',function($join){
            $join->on('car_model.id','=','model_id');
        })->where('car_detail.id',$id)->get();
        view()->share('pageTitle', 'Bid List');
        view()->share('location', 'Bid List');
       $carBids = BidList::where('user_id',auth()->id())->where('car_detail_id',$id)->get();
       
       return view('car.bidlist',['carDetail'=>$carDetail,'carBids'=>$carBids]);
    }
}
