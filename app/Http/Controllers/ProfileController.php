<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Location;
use Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        getSetting();
    }
    //
    public function index(){
        $user = Auth::user();
        return view('profile.index',compact('user'));
    }

    public function change_password(Request $request)
    {
        $message = "";
        $code = 200;
        $cur_password = $request->password;
        $user = Auth::user();

        if (Hash::check($cur_password, $user->password) ) {
            $user->password = Hash::make($request->new_password);
            $user->save();
            $message = "Password Changed Successfully !!" ;
        } else {
            $message = "Please Enter Correct Current Password !!" ;  
            $code = 400 ;
        }
        return response()->json(array( 'message' => $message), $code);
    }
}
