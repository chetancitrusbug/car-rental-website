<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarDetail;
use App\Contact;
use App\Setting;
use App\CMS;
use Auth;
use Session;
use DB;
use Mail;
use App\CarBrand;
use App\UserCasino;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        getSetting();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brandList = brandList();
        view()->share('brandList', $brandList);

        $mfgYear = getMfgYear();
        view()->share('mfgYear', $mfgYear);

        //get recent uplaoded car
        $request = new Request;
        $recentUpload = resolve('App\Http\Controllers\CarController')->getCarDetail($request,4);
        view()->share('recentUpload', $recentUpload);

        //car brands
        $brands = CarBrand::select(['id','name','logo'])->where("status","1")->get();
        view()->share('brands', $brands);

        $slider = CMS::where("slug","home-page-header")->where('status',"1")->first();
        view()->share('slider',$slider);

        $welcome = CMS::where("slug","welcome-to-autosquare")->where('status',"1")->first();
        view()->share('welcome',$welcome);

        $recentCars = CMS::where("slug","recently-added-cars")->where('status',"1")->first();
        view()->share('recentCars',$recentCars);

        $browseBrands = CMS::where("slug","browser-by-brands")->where('status',"1")->first();
        view()->share('browseBrands',$browseBrands);
        // echo '<pre>'; print_r($welcome); exit;
        return view('home');
    }

    public function aboutUs()
    {
        $setting = \App\Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);

        $aboutus = CMS::where("slug","about-us")->where('status',"1")->first();
        view()->share('aboutus',$aboutus);

        $aboutusMain = CMS::where("slug","about-us-more-than-car-dealer-websites")->where('status',"1")->first();
        view()->share('aboutusMain',$aboutusMain);

        $aboutusMission = CMS::where("slug","about-us-our-mission")->where('status',"1")->first();
        view()->share('aboutusMission',$aboutusMission);

        $whoAreWe = CMS::where("slug","who-are-we")->where('status',"1")->first();
        view()->share('whoAreWe',$whoAreWe);

        $whatDoWeDo = CMS::where("slug","what-do-we-do")->where('status',"1")->first();
        view()->share('whatDoWeDo',$whatDoWeDo);

        $whyAutoSquare = CMS::where("slug","why-autosquare")->where('status',"1")->first();
        view()->share('whyAutoSquare',$whyAutoSquare);

        return view('about-us');
    }

    public function contact()
    {
        $setting = \App\Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);

        view()->share('pageTitle', 'CONTACT');
        view()->share('location', 'Contact');

        return view('contact');
    }

    public function termsAndConditions()
    {

        view()->share('pageTitle', 'TERMS AND CONDITIONS');
        view()->share('location', 'Terms and conditions');

        $termsAndConditions = CMS::where("slug","terms-and-conditions")->where('status',"1")->first();
        view()->share('termsAndConditions',$termsAndConditions);

        return view('terms_and_conditions');
    }

    public function privacyPolicy()
    {
        view()->share('pageTitle', 'PRIVACY POLICY');
        view()->share('location', 'Privacy Policy');

        $privacyPolicy = CMS::where("slug","privacy-policy")->where('status',"1")->first();
        view()->share('privacyPolicy',$privacyPolicy);

        return view('privacy_policy');
    }

    public function contactSubmit(Request $request)
    {
        $this->validateData($request);

        $contact = new Contact;

        $contact->email = $request->get('email');
        $contact->name = $request->get('name');
        $contact->message = $request->get('message');
        $contact->title = $request->get('title',null);

        $email = Setting::select('value')->where('key',"email")->first();

        if ($contact->save()) {
            Session::flash('flash_message', 'Thank you for Contacting us.');
            //$this->mail_function->sendMailContactAction($contact);
            $subject = "Contact us Inquiry";
            $to = 'Admin';
            Mail::send('contactemail', compact( 'to', 'contact'), function ($message) use ($subject,$email) {
                $message->to($email->value)->subject($subject);
            });
            Session::flash('flash_success', 'We will get back soon !');
        } else {
            Session::flash('flash_error', 'Something wrong! Try again.');
        }
        return redirect()->back();

    }

    public function validateData($request, $id = 0)
    {
        $validation = [
            'email'    => 'required|email', // make sure the email is an actual email
            'name' => 'required',
            'message' => 'required'
        ];

        return $this->validate($request, $validation);
    }

    public function redirect()
    {
        return redirect('/admin');
    }

    public function myAccount(Request $request)
    {
        $setting = \App\Setting::pluck('value','key')->toArray();
        view()->share('setting',$setting);
        
        view()->share('pageTitle', 'MY ACCOUNT');
        view()->share('location', 'My Account');

        $carDetail = $this->getCarDetail($request,0, 0, false);

        return view('myaccount',['carDetail'=>$carDetail,'filter'=>$request->get('filter',null)]);
    }

    public function getCarDetail($request){
        $carDetail = CarDetail::select([
                'car_detail.id',
                'car_brand.name as brand_name',
                'car_model.name as model_name',
                'car_detail.type',
                'car_detail.description',
                'car_detail.condition',
                'car_detail.mfg_year',
                'car_detail.engine_size',
                'fuel_type.title as fuel_type',
                'car_detail.mileage',
                'cities.name as location',
                'car_detail.price',
                'car_bids.price as bid_price',
                'car_bids.status as bid_status',
                'car_detail.approve_bid_user_id',
                'car_detail.status',
                'car_images.image',
                DB::raw('count(car_bids.car_detail_id) as bid_count')
            ])
            ->join('car_brand',function($join){
                $join->on('car_brand.id','=','brand_id');
            })->join('car_model',function($join){
                $join->on('car_model.id','=','model_id');
            })->leftJoin('car_bids',function($join){
                $join->on('car_bids.car_detail_id','=','car_detail.id');
            })->leftJoin('car_images',function($join){
                $join->on('car_images.car_detail_id','=','car_detail.id');
            })->leftJoin('fuel_type',function($join){
                $join->on('fuel_type.id','=','car_detail.fuel_type_id');
            })->leftJoin('cities',function($join){
                $join->on('cities.id','=','car_detail.city_id');
            })
            ->whereNull('car_bids.deleted_at')
            ->where('car_bids.user_id',auth()->id())
            ->whereNull('car_images.deleted_at')
            ->where('car_detail.status',"1")
            ->groupBy('car_bids.car_detail_id');

        if($request->get('brand_id',null)){
            $carDetail->where('car_detail.brand_id',$request->get('brand_id',$brand_id));
        }

        if($request->get('model_id',0)){
            $carDetail->where('car_detail.model_id',$request->get('model_id'));
        }

        if($request->get('year',null)){
            $carDetail->where('car_detail.mfg_year',$request->get('year'));
        }

        if($request->has('filter')){
            if($request->get('filter') == '1'){
                $carDetail->orderBy('car_brand.name');
            }elseif($request->get('filter') == '2'){
                $carDetail->orderBy(DB::raw('CAST(`car_detail`.`price` AS DECIMAL(18,4))'));
            }elseif($request->get('filter') == '3'){
                $carDetail->orderBy('fuel_type.title');
            }elseif($request->get('filter') == '4'){
                $carDetail->orderBy('car_detail.mfg_year');
            }elseif($request->get('filter') == '5'){
                $carDetail->orderBy('car_model.name');
            }elseif($request->get('filter') == '6'){
                $carDetail->orderBy('car_bids.status','DESC');
            }
        }

        if($request->get('price',null)){
            $filter = config('constants.price_filter'); //mysql where condition set in condtants config
            $price = $request->get('price',null);

            $carDetail->whereRaw("car_detail.price ".$filter[$price]); //price filter from car list
        }
        /* if($recent > 0){
            $carDetail->orderBy('id','DESC')->limit(4)->get();
        } */
        return $carDetail->paginate(15);
    }
}
