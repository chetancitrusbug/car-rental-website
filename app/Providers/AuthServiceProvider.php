<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate ;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
       //parent::registerPolicies($gate);

        try {
            $masterEmail = env('MASTER_ADMIN', null);

            if (\Schema::hasTable('permissions') || auth()->check()) {
                // Dynamically register permissions with Laravel's Gate.
                foreach ($this->getPermissions() as $permission) {
                    Gate::define($permission->name, function ($user) use ($permission, $masterEmail) {

                        if (!is_null($masterEmail) && auth()->user()->email == strtolower($masterEmail)) {
                            return true;
                        }

                        return $user->hasPermission($permission);
                    });
                }
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            return;
        }

        //
    }
    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
