<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id','name','status'
    ];

    protected $table = 'car_model';

    public function brand()
    {
        return $this->hasOne('App\CarBrand','id' ,'brand_id');
    }
}
