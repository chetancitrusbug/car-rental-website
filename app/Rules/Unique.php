<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Unique implements Rule
{
    protected $table;
    protected $id;
    protected $attribute;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($table,$id=0)
    {
        $this->table = $table;
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->attribute = $attribute;
        $result = \DB::table($this->table)->where($attribute,$value);

        if($this->id > 0){
            $result->where('id','!=',$this->id);
        }
        if($this->table != 'cms'){
            $result->whereNull('deleted_at');
        }
        $result = $result->first();

        if($result){
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The '.$this->attribute.' is already exist';
    }
}
