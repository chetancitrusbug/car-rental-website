<?php
use App\CarBrand;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });



Auth::routes();

Route::get('/', 'HomeController@index')->name('/');

Route::get('/home', 'HomeController@index')->name('home');

//car module
Route::any('/car-list/{id?}', 'CarController@index')->name('car-list');
Route::get('/add-car', 'CarController@create')->name('add-car');
Route::get('/car/show/{id}', 'CarController@show')->name('car-show');
Route::post('/car-bid/{id}', 'CarController@bidStore')->name('bid-store');
Route::get('/car-model-front', 'CarController@modelList');

Route::get('/about-us', 'HomeController@aboutUs')->name('about-us');
Route::any('/contact', 'HomeController@contact')->name('contact');
Route::get('/terms-and-conditions', 'HomeController@termsAndConditions')->name('terms-and-conditions');
Route::get('/privacy-policy', 'HomeController@privacyPolicy')->name('privacy-policy');
Route::any('/contact-submit', 'HomeController@contactSubmit')->name('contact-submit');
Route::any('/myaccount', 'HomeController@myAccount')->name('my-account');

//user authentication
Route::post('/userLogin', 'LoginController@login')->name('userLogin');
Route::get('/userRegister', 'LoginController@register')->name('userRegister');
Route::post('/userRegister', 'Admin\UsersController@store')->name('registerStore');
Route::get('/userProfile', 'LoginController@userProfile')->name('user-profile');
Route::post('/updateUserProfile/{id}', 'Admin\UsersController@update')->name('user-profile-update');

Route::get('/profile/change-password', 'LoginController@changePassword')->name('front-change-password');
Route::post('/profile/change-password', 'LoginController@updatePassword')->name('front-password-update');

Route::get('/activate-account/{token}', 'Auth\RegisterController@activateAccount');

Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/home', 'HomeController@redirect');

    Route::group(['prefix' => 'admin' ], function () {

        Route::get('/', 'Admin\AdminController@index');

        //Role & Permissions
        Route::get('/give-role-permissions', 'Admin\AdminController@getGiveRolePermissions');
        Route::post('/give-role-permissions', 'Admin\AdminController@postGiveRolePermissions');
        Route::resource('/roles', 'Admin\RolesController');
        Route::resource('/permissions', 'Admin\PermissionsController');

        //Users
        Route::get('/users-data', 'Admin\UsersController@datatable');
        Route::resource('/users', 'Admin\UsersController');

        //Home CMS
        Route::get('/cms-data', 'Admin\CmsController@datatable');
        Route::resource('/cms', 'Admin\CmsController');

        //Car brand
        Route::resource('/car-brand', 'Admin\CarBrandController');
        Route::get('/car-brand-data', 'Admin\CarBrandController@datatable');

        //Car model
        Route::resource('/car-model', 'Admin\CarModelController');
        Route::get('/car-model-data', 'Admin\CarModelController@datatable');

        //Car model
        Route::resource('/car-engine', 'Admin\CarEngineController');
        Route::get('/car-engine-data', 'Admin\CarEngineController@datatable');

        //State Coach
        Route::resource('/state-coach', 'Admin\StateCoachController');
        Route::get('/state-coach-data', 'Admin\StateCoachController@datatable');

        //Abgasnorm
        Route::resource('/abgasnorm', 'Admin\AbgasnormController');
        Route::get('/abgasnorm-data', 'Admin\AbgasnormController@datatable');

        //Transmition
        Route::resource('/transmition', 'Admin\TransmitionController');
        Route::get('/transmition-data', 'Admin\TransmitionController@datatable');

        //Transmition
        Route::resource('/feature', 'Admin\FeatureController');
        Route::get('/feature-data', 'Admin\FeatureController@datatable');

        //Fuel Type
        Route::resource('/fuel-type', 'Admin\FuelTypeController');
        Route::get('/fuel-type-data', 'Admin\FuelTypeController@datatable');

        //Contact
        Route::resource('/contact', 'Admin\ContactController');
        Route::get('/contact-data', 'Admin\ContactController@datatable');

        //setting
        Route::resource('/setting', 'Admin\SettingController');
        Route::get('/setting-data', 'Admin\SettingController@datatable');

        //Fuel Type
        Route::resource('/car-detail', 'Admin\CarDetailController');
        Route::get('/car-detail-data', 'Admin\CarDetailController@datatable');
        Route::get('/car-detail-model', 'Admin\CarDetailController@modelList');
        Route::get('/car-detail-state', 'Admin\CarDetailController@statesList');
        Route::get('/car-detail-city', 'Admin\CarDetailController@citiesList');
        Route::get('/car-bid-list/{id}', 'Admin\CarDetailController@bidList');
        Route::get('/car-bid-data/{id}', 'Admin\CarDetailController@bidDatatable');
        Route::post('/car-bid-approve', 'Admin\CarDetailController@approveBid');

        //status change
        Route::get('/change-status/{table}/{status}/{id}', 'Admin\CarDetailController@changeStatus');

        //Profile
        Route::get('/profile', 'Admin\ProfileController@index')->name('profile.index');
        Route::get('/profile/edit', 'Admin\ProfileController@edit')->name('profile.edit');
        Route::patch('/profile/edit', 'Admin\ProfileController@update');
        Route::get('/profile/change-password', 'Admin\ProfileController@changePassword')->name('profile.password');
        Route::patch('/profile/change-password', 'Admin\ProfileController@updatePassword');
    });
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('/profile', 'ProfileController@index');
    Route::get('/profile/change_password', 'ProfileController@change_password');
    Route::get('/profile/change_location', 'ProfileController@change_location');
});

Route::get('/db',function(){
    try {
        DB::connection()->getPdo();
        if(DB::connection()->getDatabaseName()){
            echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
        }
    } catch (\Exception $e) {
        die("Could not connect to the database.  Please check your configuration.");
    }
});

function getMfgYear(){
    $year = [];
    $currentYear = \Carbon\Carbon::now()->format('Y');
    for ($i = config('constants.car_detail.year.start'); $i <= $currentYear; $i++) {
        $year[$i] = $i;
    }
    return $year;
}

function brandList(){
    return CarBrand::where("status","1")->pluck('name','id')->toArray();
}

