<?php

return [

    'machine_offered' => [
        0 => "Not Offered",
        1 => "Offered" 
    ]

];

/*


Default status - 0 for location, casino table and 3 for user_casino table

ALTER TABLE `casino` ADD `created_by` INT NULL DEFAULT NULL AFTER `deleted_at`;

ALTER TABLE `user_casino` ADD `ref_id` INT NULL DEFAULT NULL AFTER `casino_id`;

ALTER TABLE `user_casino` CHANGE `status` `status` INT(11) NOT NULL DEFAULT '3';

ALTER TABLE `location` ADD `user_id` INT NULL DEFAULT NULL AFTER `updated_at`;

ALTER TABLE `users` CHANGE `status` `status` VARCHAR(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '1';

when user makes a entry of new casino it will be of status = 3 
request sent to admin   
if admin accepts the request of new casino , then status = 0
if admin cancels the request of new casino , then status = 4

if any user update the casino data form forntend it will be of status = 1
request sent to admin
if admin accepts the request of update casino data , then status = 0
if admin cancels the request of update casino data , then status = 2

*/