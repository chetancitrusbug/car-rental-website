$(document).ready( function(){

    /********************************
    *           Customizer          *
    ********************************/
    var body = $('body'),
    default_bg_color = $('.app-sidebar').attr('data-background-color'),
    default_bg_image = $('.app-sidebar').attr('data-image');

    $('.cz-bg-color span[data-bg-color="'+default_bg_color+'"]').addClass('selected');
    $('.cz-bg-image img[src$="'+default_bg_image+'"]').addClass('selected');

    // Customizer toggle & close button click events  [Remove customizer code from production]
    $('.customizer-toggle').on('click',function(){
        $('.customizer').toggleClass('open');
    });
    $('.customizer-close').on('click',function(){
        $('.customizer').removeClass('open');
    });
    if($('.customizer-content').length > 0){
        $('.customizer-content').perfectScrollbar({
            theme:"dark"
        });
    }

    // Change Sidebar Background Color
    $('.cz-bg-color span').on('click',function(){
        var $this = $(this),
        bgColor = $this.attr('data-bg-color');

        $this.closest('.cz-bg-color').find('span.selected').removeClass('selected');
        $this.addClass('selected');

        $('.app-sidebar').attr('data-background-color', bgColor);
        if(bgColor == 'white'){
            $('.logo-img img').attr('src','../../app-assets/img/logo-dark.png');
        }
        else{
            if($('.logo-img img').attr('src') == '../../app-assets/img/logo-dark.png'){
                $('.logo-img img').attr('src','../../app-assets/img/logo.png');
            }
        }
    });

    // Change Background Image
    $('.cz-bg-image img').on('click',function(){
        var $this = $(this),
        src = $this.attr('src');

        $('.sidebar-background').css('background-image', 'url(' + src + ')');
        $this.closest('.cz-bg-image').find('.selected').removeClass('selected');
        $this.addClass('selected');
    });

    $('.cz-bg-image-display').on('click',function(){
        var $this = $(this);
        if($this.prop('checked') === true){
            $('.sidebar-background').css('display','block');
        }
        else{
            $('.sidebar-background').css('display','none');
        }
    });

    $('.cz-compact-menu').on('click',function(){
        $('.nav-toggle').trigger('click');
        if($(this).prop('checked') === true){
            $('.app-sidebar').trigger('mouseleave');
        }
    });

    $('.cz-sidebar-width').on('change',function(){
        var $this = $(this),
        width_val = this.value,
        wrapper = $('.wrapper');

        if(width_val === 'small'){
            $(wrapper).removeClass('sidebar-lg').addClass('sidebar-sm');
        }
        else if(width_val === 'large'){
            $(wrapper).removeClass('sidebar-sm').addClass('sidebar-lg');
        }
        else{
            $(wrapper).removeClass('sidebar-sm sidebar-lg');
        }

    });

});

(function() {
    var laravel = {
        initialize: function() {
            this.methodLinks = $('body');
            this.registerEvents();
        },
        registerEvents: function() {
            //this.methodLinks.on('click', this.handleMethod);
            this.methodLinks.on('click', 'a[data-method]', this.handleMethod);
        },
        handleMethod: function(e) {
            e.preventDefault();
            var link = $('.del-item');
            var httpMethod = link.data('method').toUpperCase();
            var allowedMethods = ['PUT', 'DELETE', 'GET'];
            var extraMsg = link.data('modal-text');
            var url = link.attr('href')

            var msg = '<i class="fa fa-exclamation-triangle fa-2x" style="vertical-align: middle; color:#f39c12;"></i> &nbsp;Are you sure you want to&nbsp;' + extraMsg;
            // If the data-method attribute is not PUT or DELETE,
            // then we don't know what to do. Just ignore.
            if ($.inArray(httpMethod, allowedMethods) === -1) {
                return;
            }
            return;
            bootbox.dialog({
                message: msg,
                title: "Please Confirm",
                buttons: {
                    success: {
                        label: "Cancel",
                        className: "btn-default",
                        callback: function() {}
                    },
                    danger: {
                        label: "OK",
                        className: "btn-success",
                        callback: function() {
                            var form = $('<form>', {
                                'method': 'POST',
                                'action': link.attr('href')
                            });
                            var hiddenInput = $('<input>', {
                                'name': '_method',
                                'type': 'hidden',
                                'value': link.data('method')
                            });
                            form.append(hiddenInput).appendTo('body').submit();
                        }
                    }
                }
            });
        }
    };
    laravel.initialize();
})();

function changeImage(id){
    $('.changeImage'+id).hide();
}

jQuery(document).ready(function($) {
    statusChange();
});

function statusChange() {
    $('.status').checkboxpicker({
        offLabel: 'Inactive',
        onLabel: 'Active',
    });
    $('.status1').checkboxpicker();
}
