/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.40 : Database - tablegambler
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tablegambler` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `tablegambler`;

/*Table structure for table `casino` */

DROP TABLE IF EXISTS `casino`;

CREATE TABLE `casino` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `casino` */

insert  into `casino`(`id`,`name`,`location_id`,`status`,`created_at`,`updated_at`,`deleted_at`,`created_by`) values (1,'Casino Royale','4',1,'2018-07-20 09:24:02','2018-07-20 09:24:02',NULL,1),(2,'Cosmopolitan','5',1,'2018-07-20 09:28:38','2018-07-20 09:28:38',NULL,1),(3,'Casino Royale','6',1,'2018-07-20 09:31:52','2018-07-20 09:31:52',NULL,1),(4,'Bally\'s Bellagio','11',1,'2018-07-20 09:41:54','2018-07-20 09:41:54',NULL,3);

/*Table structure for table `dropdown_type` */

DROP TABLE IF EXISTS `dropdown_type`;

CREATE TABLE `dropdown_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `dropdown_type` */

/*Table structure for table `dropdown_value` */

DROP TABLE IF EXISTS `dropdown_value`;

CREATE TABLE `dropdown_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `dropdown_value` */

/*Table structure for table `location` */

DROP TABLE IF EXISTS `location`;

CREATE TABLE `location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `location` */

insert  into `location`(`id`,`name`,`status`,`created_at`,`updated_at`,`user_id`) values (1,'Thailand',1,'2018-07-20 09:14:46','2018-07-20 09:14:46',NULL),(4,'Philliphines',1,'2018-07-20 09:18:04','2018-07-20 09:18:12',NULL),(5,'Canada',1,'2018-07-20 09:18:37','2018-07-20 09:18:37',NULL),(6,'USA',1,'2018-07-20 09:18:44','2018-07-20 09:18:44',NULL),(7,'Australia',1,'2018-07-20 09:18:56','2018-07-20 09:18:56',NULL),(8,'Singapore',1,'2018-07-20 09:20:20','2018-07-20 09:20:20',NULL),(9,'Dubai',0,'2018-07-20 09:20:48','2018-07-20 09:20:48',NULL),(10,'Germany',0,'2018-07-20 09:21:11','2018-07-20 09:21:11',NULL),(11,'Russia',1,'2018-07-20 09:21:18','2018-07-20 09:21:18',NULL),(12,'France',1,'2018-07-20 09:21:29','2018-07-20 09:21:29',NULL),(13,'Italy',1,'2018-07-20 09:22:48','2018-07-20 09:22:48',NULL),(14,'Austria',1,'2018-07-20 09:37:50','2018-07-20 09:37:50',NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_193651_create_roles_permissions_tables',1),(4,'2018_06_22_123310_create_pages_table',1),(5,'2018_06_22_123334_create_location_table',1),(6,'2018_06_22_123353_create_casino_table',1),(7,'2018_06_22_123415_create_user_casino_table',1),(8,'2018_06_22_123519_create_dropdown_type_table',1),(9,'2018_06_22_123537_create_dropdown_value_table',1),(10,'2018_07_18_075656_add_status_in_user_casino',2);

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pages` */

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`parent_id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,0,'access.users','Can Access Users',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(2,1,'access.user.create','Can Create User',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(3,1,'access.user.edit','Can Edit User',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(4,1,'access.user.delete','Can Delete User',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(5,0,'access.roles','Can Access Roles',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(6,5,'access.role.create','Can Create Role',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(7,5,'access.role.edit','Can Edit Role',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(8,5,'access.role.delete','Can Delete Role',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(9,0,'access.permissions','Can Access Permission',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(10,9,'access.permission.create','Can Create Permission',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(11,9,'access.permission.edit','Can Edit Permission',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37'),(12,9,'access.permission.delete','Can Delete Permission',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37');

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`role_id`,`user_id`) values (1,1),(1,3);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,'SU','Super User',NULL,'2018-07-17 05:17:37','2018-07-17 05:17:37');

/*Table structure for table `user_casino` */

DROP TABLE IF EXISTS `user_casino`;

CREATE TABLE `user_casino` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `casino_id` int(11) NOT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `min_bet` double(8,2) NOT NULL DEFAULT '0.00',
  `max_bet` double(8,2) NOT NULL DEFAULT '0.00',
  `machine_offered` int(11) DEFAULT NULL,
  `min_odds` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `odds` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_data_observed` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '3',
  `approved_on` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_on` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cancelled_by` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_casino` */

insert  into `user_casino`(`id`,`user_id`,`casino_id`,`ref_id`,`min_bet`,`max_bet`,`machine_offered`,`min_odds`,`odds`,`date_data_observed`,`created_at`,`updated_at`,`status`,`approved_on`,`approved_by`,`cancelled_on`,`cancelled_by`) values (1,1,1,NULL,2000.00,5000.00,1,'1','10','2018-07-12','2018-07-20 09:24:02','2018-07-20 09:27:10',0,NULL,NULL,NULL,NULL),(2,1,2,NULL,1000.00,2000.00,0,NULL,NULL,'2018-07-28','2018-07-20 09:28:38','2018-07-20 09:28:38',0,NULL,NULL,NULL,NULL),(3,1,3,NULL,100.00,200.00,0,NULL,NULL,'2018-07-26','2018-07-20 09:31:52','2018-07-20 09:31:52',0,NULL,NULL,NULL,NULL),(4,3,3,3,100.00,200.00,1,'2',NULL,'2018-07-26','2018-07-20 09:39:24','2018-07-20 09:39:24',1,NULL,NULL,NULL,NULL),(5,3,3,3,100.00,200.00,1,'2',NULL,'2018-07-26','2018-07-20 09:39:39','2018-07-20 09:39:39',1,NULL,NULL,NULL,NULL),(6,3,4,6,5000.00,7000.00,1,'15','2','2018-07-10','2018-07-20 09:41:54','2018-07-20 09:42:19',0,'20-07-2018','1',NULL,NULL);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`location_id`,`facebook_id`,`google_id`,`activation_token`,`status`,`remember_token`,`created_at`,`updated_at`,`deleted_at`) values (1,'Admins','admin.citrusbug@gmail.com','$2y$10$yZ4QX.W1oGZOjQglpB6yw.ZbAPE9hG/dvGdyuML3/v84UvmC726a6',NULL,NULL,NULL,NULL,NULL,'3E02z5v5tansR7sgoiUBuWJyA7kITgh4SzOzH1w7zMYgHaOdFvR1VlFLrEVD','2018-07-17 05:17:37','2018-07-20 09:14:30',NULL),(2,'vraj1607','vraj.citrusbug@gmail.com','$2y$10$fRArlEXgm/51bCKGXLF3muaZdGK4p.I8vOJnhgoej8XbZ2R4eVgEm','1',NULL,NULL,'0f3cb7dec860e8f776789e06b663fc54a34fbefd',NULL,'vwu1AWJAlZhcZM0TOaA7nBcFVTUCpWNiwbfkMma9aTD8YFOdyYGoOa8AlvcD','2018-07-20 09:37:01','2018-07-20 09:47:46',NULL),(3,'snehals','snehal.citrusbug@gmail.com','$2y$10$V9tHyJQPR2zHp/mVinJmLOxTDEIADOxSyoZbkiq/2/hEUoLW8.Ge2','14',NULL,NULL,'cdd08d48a732eb87e20b1303635db76d6e72aa17','0','TIhqcjJPZ8MvsFRSo64iHwkTncm6TS4G9KjanURNzMewVGjfaKQ98mNgCO7u','2018-07-20 09:38:56','2018-07-20 09:43:27',NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
